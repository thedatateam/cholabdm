export function getCurrentMonth() {
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  return months[new Date().getMonth()];
}

export function formatNumbers(params) {
  return parseFloat(params.value).toFixed(2);
}
