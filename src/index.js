import React from 'react';
import ReactDOM from 'react-dom';
import reduxStore from './store';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import './assets/styles/custom.scss';
import CholaBDM from './containers';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Provider store={reduxStore.store}>
    <ConnectedRouter history={reduxStore.history}>
            <CholaBDM />
    </ConnectedRouter>
</Provider>, document.getElementById('root'));

serviceWorker.register();
