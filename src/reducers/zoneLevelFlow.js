import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function zoneManagerFlowReducer(state = initialState.zoneManager, action) {
    switch (action.type) {
        case types.GET_ZONE_LEVEL_SUMMARY_INITIATED:
            return {
                getZoneLevelSummaryPending: true,
                error: null,
                getZoneLevelSummaryError: false,
                getZoneLevelSummarySuccess: false
            };
        case types.GET_ZONE_LEVEL_SUMMARY_FAILURE:
            return {
                getZoneLevelSummaryPending: false,
                error: action.error,
                getZoneLevelSummaryError: true,
                getZoneLevelSummarySuccess: false
            };
        case types.GET_ZONE_LEVEL_SUMMARY_SUCCESS:
            return {
                getZoneLevelSummaryPending: false,
                error: null,
                getZoneLevelSummarySuccess: true,
                response: action.zoneManager,
                getZoneLevelSummaryError: false
            };
        case types.GET_ZONE_FINALIZED_RATE_INITIATED:
            return {
                getZoneFinalizedRatePending: true,
                error: null,
                getZoneFinalizedRateError: false,
                getZoneFinalizedRateSuccess: false
            };
        case types.GET_ZONE_FINALIZED_RATE_FAILURE:
            return {
                getZoneFinalizedRatePending: false,
                error: action.error,
                getZoneFinalizedRateError: true,
                getZoneFinalizedRateSuccess: false
            };
        case types.GET_ZONE_FINALIZED_RATE_SUCCESS:
            return {
                getZoneFinalizedRatePending: false,
                error: null,
                getZoneFinalizedRateSuccess: true,
                response: action.zoneManager,
                getZoneFinalizedRateError: false
            };
        case types.GET_ZONE_PRODUCT_LEVEL_INITIATED:
            return {
                zoneProductInputPending: true,
                error: null,
                zoneProductInputError: false,
                zoneProductInputSuccess: false
            };
        case types.GET_ZONE_PRODUCT_LEVEL_FAILURE:
            return {
                zoneProductInputPending: false,
                error: action.error,
                zoneProductInputError: true,
                zoneProductInputSuccess: false
            };
        case types.GET_ZONE_PRODUCT_LEVEL_SUCCESS:
            return {
                zoneProductInputPending: false,
                error: null,
                zoneProductInputSuccess: true,
                response: action.zoneProductInput,
                zoneProductInputError: false
            };
        case types.PUT_ZONE_PRODUCT_LEVEL_UPDATE_INITIATED:
            return {
                updateZoneProductLevelOutputPending: true,
                error: null,
                updateZoneProductLevelOutputError: false,
                updateZoneProductLevelOutputSuccess: false
            };
        case types.PUT_ZONE_PRODUCT_LEVEL_UPDATE_FAILURE:
            return {
                updateZoneProductLevelOutputPending: false,
                error: action.error,
                updateZoneProductLevelOutputError: true,
                updateZoneProductLevelOutputSuccess: false
            };
        case types.PUT_ZONE_PRODUCT_LEVEL_UPDATE_SUCCESS:
            return {
                updateZoneProductLevelOutputPending: false,
                error: null,
                updateZoneProductLevelOutputSuccess: true,
                response: action.updateZoneProductInput,
                updateZoneProductLevelOutputError: false
            };
        case types.GET_GRAPH_DATA_INITIATED:
            return {
                graphDataPending: true,
                error: null,
                graphDataError: false,
                graphDataSuccess: false
            };
        case types.GET_GRAPH_DATA_FAILURE:
            return {
                graphDataPending: false,
                error: action.error,
                graphDataError: true,
                graphDataSuccess: false
            };
        case types.GET_GRAPH_DATA_SUCCESS:
            return {
                graphDataPending: false,
                error: null,
                graphDataSuccess: true,
                response: action.graphData,
                graphDataError: false
            };
        default:
            return state;
    }
}