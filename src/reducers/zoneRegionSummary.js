import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function zoneRegionSummaryReducer(
  state = initialState.regionManager,
  action
) {
  switch (action.type) {
    case types.GET_ZM_REGION_LEVEL_SUMMARY_INITIATED:
      return {
        ...state,
        statusZM: 'PENDING',
      };
    case types.GET_ZM_REGION_LEVEL_SUMMARY_SUCCESS:
      return {
        ...state,
        statusZM: 'SUCCESS',
        regionDataZM: action.data,
      };
    case types.GET_ZM_REGION_LEVEL_SUMMARY_FAILURE:
      return {
        ...state,
        statusZM: 'FAILURE',
        error: action.error,
      };
    default:
      return state;
  }
}
