import initialState from './initialState';
import * as types from '../actions/actionTypes';

function submitForApprovalZone(state = initialState.submitForApprovalZone, action) {
  switch (action.type) {
    case types.SUBMIT_FOR_APPROVAL_ZM_INITIATED:
      return { ...state, status: 'PENDING' };
    case types.SUBMIT_FOR_APPROVAL_ZM_SUCCESS:
      return { ...state, data: action.data, status: 'SUCCESS' };
    case types.SUBMIT_FOR_APPROVAL_ZM_FAILURE:
      return { ...state, error: action.error, status: 'FAILURE' };
    default:
      return state;
  }
}

export default submitForApprovalZone;
