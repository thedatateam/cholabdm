import user from './login';
import areaManager from './areaManagerFlow';
import assetTypeView from './assetTypeView';
import submitForApproval from './submitForApproval';
import submitForApprovalZone from './submitForApprovalZone';
import assetDetailedView from './assetDetailedView';
import regionFinalisedRate from './regionFinalisedRate';
import areaLevelSummary from './areaLevelSummary';
import zoneManager from './zoneLevelFlow';
import regionManager from './regionLevelFlow';
import zoneRegionSummary from './zoneRegionSummary';

export default {
  user,
  areaManager,
  assetTypeView,
  regionFinalisedRate,
  submitForApproval,
  submitForApprovalZone,
  areaLevelSummary,
  zoneRegionSummary,
  assetDetailedView,
  zoneManager,
  regionManager,
};
