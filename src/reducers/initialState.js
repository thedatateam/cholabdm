export default {
  user: [],
  userdetails: [],
  areaManager: [],
  regionManager:[],
  zoneManager:[],
  assetTypeView: {
    status: 'IDLE', // IDLE, PENDING, SUCCESS, FAILURE
    rmStatus: 'IDLE',
    zmStatus: 'IDLE',
    allData: [],
    newData: [],
    shubhData: [],
    usedData: [],
    error: null,
  },
  regionFinalisedRate: {
    status: 'IDLE', // IDLE, PENDING, SUCCESS, FAILURE
    error: null,
    data: null,
  },
  areaLevelSummary: {
    statusRM: 'IDLE', // IDLE, PENDING, SUCCESS, FAILURE
    dataRM: null,
    statusZM: 'IDLE',
    dataZM: null,
    error: null,
  },
  submitForApproval: {
    status: 'IDLE', // IDLE, PENDING, SUCCESS, FAILURE
    statusRM: 'IDLE',
    dataRM: null,
    statusRRM: 'IDLE',
    dataRRM: null,
    statusRRCM: 'IDLE',
    dataRRCM: null,
    error: null,
    data: null,
  },
  submitForApprovalZone: {
    status: 'IDLE', // IDLE, PENDING, SUCCESS, FAILURE
    error: null,
    data: null
  },
  assetDetailedView: {
    status: 'IDLE', // IDLE, PENDING, SUCCESS, FAILURE
    error: null,
    data: null,
  },
};
