import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function regionManagerFlowReducer(state = initialState.regionManager, action) {
    switch (action.type) {
        case types.GET_REGION_LEVEL_SUMMARY_INITIATED:
            return {
                getRegionLevelSummaryPending: true,
                error: null,
                getRegionLevelSummaryError: false,
                getRegionLevelSummarySuccess: false
            };
        case types.GET_REGION_LEVEL_SUMMARY_FAILURE:
            return {
                getRegionLevelSummaryPending: false,
                error: action.error,
                getRegionLevelSummaryError: true,
                getRegionLevelSummarySuccess: false
            };
        case types.GET_REGION_LEVEL_SUMMARY_SUCCESS:
            return {
                getRegionLevelSummaryPending: false,
                error: null,
                getRegionLevelSummarySuccess: true,
                response: action.regionManager,
                getRegionLevelSummaryError: false
            };
        case types.GET_REGION_PRODUCT_LEVEL_INITIATED:
            return {
                regionProductInputPending: true,
                error: null,
                regionProductInputError: false,
                regionProductInputSuccess: false
            };
        case types.GET_REGION_PRODUCT_LEVEL_FAILURE:
            return {
                regionProductInputPending: false,
                error: action.error,
                regionProductInputError: true,
                regionProductInputSuccess: false
            };
        case types.GET_REGION_PRODUCT_LEVEL_SUCCESS:
            return {
                regionProductInputPending: false,
                error: null,
                regionProductInputSuccess: true,
                response: action.regionProductInput,
                regionProductInputError: false
            };
        case types.PUT_REGION_PRODUCT_LEVEL_UPDATE_INITIATED:
            return {
                updateRegionProductLevelOutputPending: true,
                error: null,
                updateRegionProductLevelOutputError: false,
                updateRegionProductLevelOutputSuccess: false
            };
        case types.PUT_REGION_PRODUCT_LEVEL_UPDATE_FAILURE:
            return {
                updateRegionProductLevelOutputPending: false,
                error: action.error,
                updateRegionProductLevelOutputError: true,
                updateRegionProductLevelOutputSuccess: false
            };
        case types.PUT_REGION_PRODUCT_LEVEL_UPDATE_SUCCESS:
            return {
                updateRegionProductLevelOutputPending: false,
                error: null,
                updateRegionProductLevelOutputSuccess: true,
                response: action.updateRegionProductInput,
                updateRegionProductLevelOutputError: false
            };
        case types.GET_GRAPH_DATA_INITIATED:
            return {
                graphDataPending: true,
                error: null,
                graphDataError: false,
                graphDataSuccess: false
            };
        case types.GET_GRAPH_DATA_FAILURE:
            return {
                graphDataPending: false,
                error: action.error,
                graphDataError: true,
                graphDataSuccess: false
            };
        case types.GET_GRAPH_DATA_SUCCESS:
            return {
                graphDataPending: false,
                error: null,
                graphDataSuccess: true,
                response: action.graphData,
                graphDataError: false
            };
        case types.GET_ZM_REGION_LEVEL_SUMMARY_INITIATED:
            return {
                ...state,
                statusZM: 'PENDING',
            }
        case types.GET_ZM_REGION_LEVEL_SUMMARY_SUCCESS:
            return {
                ...state,
                statusZM: 'SUCCESS',
                regionDataZM: action.data
            }
        case types.GET_ZM_REGION_LEVEL_SUMMARY_FAILURE:
            return {
                ...state,
                statusZM: 'FAILURE',
                error: action.error
            }
        default:
            return state;
    }
}