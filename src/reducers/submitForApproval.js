import initialState from './initialState';
import * as types from '../actions/actionTypes';

function submitForApproval(state = initialState.submitForApproval, action) {
  switch (action.type) {
    case types.SUBMIT_FOR_APPROVAL_INITIATED:
      return { ...state, status: 'PENDING' };
    case types.SUBMIT_FOR_APPROVAL_SUCCESS:
      return { ...state, data: action.data, status: 'SUCCESS' };
    case types.SUBMIT_FOR_APPROVAL_FAILURE:
      return { ...state, error: action.error, status: 'FAILURE' };
    case types.SUBMIT_FOR_APPROVAL_RM_INITIATED:
      return { ...state, statusRM: 'PENDING' };
    case types.SUBMIT_FOR_APPROVAL_RM_SUCCESS:
      return { ...state, dataRM: action.data, statusRM: 'SUCCESS' };
    case types.SUBMIT_FOR_APPROVAL_RM_FAILURE:
      return { ...state, error: action.error, statusRM: 'FAILURE' };
    case types.SUBMIT_FOR_REWORK_RM_INITIATED:
      return { ...state, statusRRM: 'PENDING' };
    case types.SUBMIT_FOR_REWORK_RM_SUCCESS:
      return { ...state, dataRRM: action.data, statusRRM: 'SUCCESS' };
    case types.SUBMIT_FOR_REWORK_RM_FAILURE:
      return { ...state, error: action.error, statusRRM: 'FAILURE' };
    case types.SUBMIT_FOR_REWORK_COMMENT_RM_INITIATED:
      return { ...state, statusRRCM: 'PENDING' };
    case types.SUBMIT_FOR_REWORK_COMMENT_RM_SUCCESS:
      return { ...state, dataRRCM: action.data, statusRRCM: 'SUCCESS' };
    case types.SUBMIT_FOR_REWORK_COMMENT_RM_FAILURE:
      return { ...state, error: action.error, statusRRCM: 'FAILURE' };
    default:
      return state;
  }
}

export default submitForApproval;
