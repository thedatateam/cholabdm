import initialState from './initialState';
import * as types from '../actions/actionTypes';

function regionFinalisedRate(state = initialState.regionFinalisedRate, action) {
  switch (action.type) {
    case types.GET_RM_FINALIIZED_RATE_INITIATED:
      return { ...state, status: 'PENDING', data: {FinalizedRate: [], totalPages: 0} };
    case types.GET_RM_FINALIIZED_RATE_SUCCESS:
      return { ...state, status: 'SUCCESS', data: action.data };
    case types.GET_RM_FINALIIZED_RATE_FAILURE:
      return { ...state, error: action.error, status: 'FAILURE', data: {FinalizedRate: [], totalPages: 0} };
    default:
      return state;
  }
}

export default regionFinalisedRate;
