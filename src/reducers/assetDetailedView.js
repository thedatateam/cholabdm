import initialState from './initialState';
import * as types from '../actions/actionTypes';

function assetDetailedView(state = initialState.assetDetailedView, action) {
  switch (action.type) {
    case types.GET_ASSET_DETAILED_VIEW_INITIATED:
      return { ...state, status: 'PENDING', data: [] };
    case types.GET_ASSET_DETAILED_VIEW_SUCCESS:
      return { ...state, status: 'SUCCESS', data: action.data };
    case types.GET_ASSET_DETAILED_VIEW_FAILURE:
      return { ...state, error: action.error, status: 'FAILURE', data: [] };
    case types.GET_RM_ASSET_DETAILED_VIEW_INITIATED:
      return { ...state, status: 'PENDING', data: [] };
    case types.GET_RM_ASSET_DETAILED_VIEW_SUCCESS:
      return { ...state, status: 'SUCCESS', data: action.data };
    case types.GET_RM_ASSET_DETAILED_VIEW_FAILURE:
      return { ...state, error: action.error, status: 'FAILURE', data: [] };
    case types.GET_ZM_ASSET_DETAILED_VIEW_INITIATED:
      return { ...state, status: 'PENDING', data: [] };
    case types.GET_ZM_ASSET_DETAILED_VIEW_SUCCESS:
      return { ...state, status: 'SUCCESS', data: action.data };
    case types.GET_ZM_ASSET_DETAILED_VIEW_FAILURE:
      return { ...state, error: action.error, status: 'FAILURE', data: [] };
    default:
      return state;
  }
}

export default assetDetailedView;
