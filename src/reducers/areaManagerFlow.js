import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function areaMAnagerFlowReducer(state = initialState.areaManager, action) {
    switch (action.type) {
        case types.GET_AREA_LEVEL_SUMMARY_INITIATED:
            return {
                ...state,
                getAreaLevelSummaryPending: true,
                error: null,
                getAreaLevelSummaryError: false,
                getAreaLevelSummarySuccess: false
            };
        case types.GET_AREA_LEVEL_SUMMARY_FAILURE:
            return {
                ...state,
                getAreaLevelSummaryPending: false,
                error: action.error,
                getAreaLevelSummaryError: true,
                getAreaLevelSummarySuccess: false
            };
        case types.GET_AREA_LEVEL_SUMMARY_SUCCESS:
            return {
                ...state,
                getAreaLevelSummaryPending: false,
                error: null,
                getAreaLevelSummarySuccess: true,
                response: action.areaManager,
                getAreaLevelSummaryError: false
            };
        case types.GET_FINALIIZED_RATE_INITIATED:
            return {
                ...state,
                getFinalizedRatePending: true,
                error: null,
                getFinalizedRateError: false,
                getFinalizedRateSuccess: false
            };
        case types.GET_FINALIIZED_RATE_FAILURE:
            return {
                ...state,
                getFinalizedRatePending: false,
                error: action.error,
                getFinalizedRateError: true,
                getFinalizedRateSuccess: false
            };
        case types.GET_FINALIIZED_RATE_SUCCESS:
            return {
                ...state,
                getFinalizedRatePending: false,
                error: null,
                getFinalizedRateSuccess: true,
                response: action.finalizedRate,
                getFinalizedRateError: false
            };
        case types.GET_PRODUCT_LEVEL_INITIATED:
            return {
                ...state,
                productLevelOutputPending: true,
                error: null,
                productLevelOutputError: false,
                productLevelOutputSuccess: false
            };
        case types.GET_PRODUCT_LEVEL_FAILURE:
            return {
                ...state,
                productLevelOutputPending: false,
                error: action.error,
                productLevelOutputError: true,
                productLevelOutputSuccess: false
            };
        case types.GET_PRODUCT_LEVEL_SUCCESS:
            return {
                ...state,
                productLevelOutputPending: false,
                error: null,
                productLevelOutputSuccess: true,
                response: action.productInput,
                productLevelOutputError: false
            };
        case types.PUT_PRODUCT_LEVEL_UPDATE_INITIATED:
            return {
                ...state,
                updateProductLevelOutputPending: true,
                error: null,
                updateProductLevelOutputError: false,
                updateProductLevelOutputSuccess: false
            };
        case types.PUT_PRODUCT_LEVEL_UPDATE_FAILURE:
            return {
                ...state,
                updateProductLevelOutputPending: false,
                error: action.error,
                updateProductLevelOutputError: true,
                updateProductLevelOutputSuccess: false
            };
        case types.PUT_PRODUCT_LEVEL_UPDATE_SUCCESS:
            return {
                ...state,
                updateProductLevelOutputPending: false,
                error: null,
                updateProductLevelOutputSuccess: true,
                response: action.updateProductInput,
                updateProductLevelOutputError: false
            };
        case types.GET_NEW_ASSET_VIEW_INITIATED:
            return {
                ...state,
                getNewAssetViewPending: true,
                error: null,
                getNewAssetViewError: false,
                getNewAssetViewSuccess: false
            };
        case types.GET_NEW_ASSET_VIEW_FAILURE:
            return {
                ...state,
                getNewAssetViewPending: false,
                error: action.error,
                getNewAssetViewError: true,
                getNewAssetViewSuccess: false
            };
        case types.GET_NEW_ASSET_VIEW_SUCCESS:
            return {
                ...state,
                getNewAssetViewPending: false,
                error: null,
                getNewAssetViewSuccess: true,
                response: action.newAssetType,
                getNewAssetViewError: false
            };
            case types.GET_BDMT_TEMPLATE_STATUS_INITIATED:
            return {
                ...state,
                getBDMTemplateStatusPending: true,
                error: null,
                getBDMTemplateStatusError: false,
                getBDMTemplateStatusSuccess: false
            };
        case types.GET_BDMT_TEMPLATE_STATUS_FAILURE:
            return {
                ...state,
                getBDMTemplateStatusPending: false,
                error: action.error,
                getBDMTemplateStatusError: true,
                getBDMTemplateStatusSuccess: false
            };
        case types.GET_BDMT_TEMPLATE_STATUS_SUCCESS:
            return {
                ...state,
                getBDMTemplateStatusPending: false,
                error: null,
                getBDMTemplateStatusSuccess: true,
                responseBDM: action.templateStatus,
                getBDMTemplateStatusError: false
            }
        case types.GET_SEGMENT_GRAPH_INITIATED:
            return {
                ...state,
                getCustomerSegmentGraphDataPending: true,
                error: null,
                getCustomerSegmentGraphDataError: false,
                getCustomerSegmentGraphDataSuccess: false
            };
        case types.GET_SEGMENT_GRAPH_FAILURE:
            return {
                ...state,
                getCustomerSegmentGraphDataPending: false,
                error: action.error,
                getCustomerSegmentGraphDataError: true,
                getCustomerSegmentGraphDataSuccess: false
            };
        case types.GET_SEGMENT_GRAPH_SUCCESS:
            return {
                ...state,
                getCustomerSegmentGraphDataPending: false,
                error: null,
                getCustomerSegmentGraphDataSuccess: true,
                responseCategoryGraphData:[],
                responseGraphData: action.segmentGraphData,
                getCustomerSegmentGraphDataError: false
            };
            case types.GET_CATEGORY_GRAPH_INITIATED:
            return {
                ...state,
                getCustomerCategoryGraphDataPending: true,
                error: null,
                getCustomerCategoryGraphDataError: false,
                getCustomerCategoryGraphDataSuccess: false
            };
        case types.GET_CATEGORY_GRAPH_FAILURE:
            return {
                ...state,
                getCustomerCategoryGraphDataPending: false,
                error: action.categoryGraphData,
                getCustomerCategoryGraphDataError: true,
                getCustomerCategoryGraphDataSuccess: false
            };
        case types.GET_CATEGORY_GRAPH_SUCCESS:
            return {
                ...state,
                getCustomerCategoryGraphDataPending: false,
                error: null,
                getCustomerCategoryGraphDataSuccess: true,
                responseGraphData:[],
                responseCategoryGraphData: action.categoryGraphData,
                getCustomerCategoryGraphDataError: false
            };
            case types.GET_ALL_DROP_DOWN_INITIATED:
            return {
                ...state,
                getALLDropDownPending: true,
                error: null,
                getALLDropDownError: false,
                getALLDropDownSuccess: false
            };
        case types.GET_ALL_DROP_DOWN_FAILURE:
            return {
                ...state,
                getALLDropDownwPending: false,
                error: action.error,
                getALLDropDownError: true,
                getALLDropDownSuccess: false
            };
        case types.GET_ALL_DROP_DOWN_SUCCESS:
            return {
                ...state,
                getALLDropDownPending: false,
                error: null,
                getALLDropDownSuccess: true,
                responseData: action.allDropdown,
                getALLDropDownError: false
            }; 
            case types.SEARCH_FINALIZED_RATE_INITIATED:
                return {
                    ...state,
                    searchFinalizedRatePending: true,
                    error: null,
                    searchFinalizedRateError: false,
                    searchFinalizedRateSuccess: false
                };
            case types.SEARCH_FINALIZED_RATE_FAILURE:
                return {
                    ...state,
                    searchFinalizedRatePending: false,
                    error: action.error,
                    searchFinalizedRateError: true,
                    searchFinalizedRateSuccess: false
                };
            case types.SEARCH_FINALIZED_RATE_SUCCESS:
                return {
                    ...state,
                    searchFinalizedRatePending: false,
                    error: null,
                    searchFinalizedRateSuccess: true,
                    response: action.searchfinalizedRate,
                    searchFinalizedRateError: false
                };
        default:
            return state;
    }
}