import * as types from '../actions/actionTypes';
import initialState from './initialState';

function areaLevelSummary(state = initialState.areaLevelSummary, action) {
  switch (action.type) {
    case types.GET_RM_AREA_LEVEL_SUMMARY_INITIATED:
      return { ...state, statusRM: 'PENDING' };
    case types.GET_RM_AREA_LEVEL_SUMMARY_FAILURE:
      return { ...state, statusRM: 'FAILURE', error: action.error };
    case types.GET_RM_AREA_LEVEL_SUMMARY_SUCCESS:
      return { ...state, statusRM: 'SUCCESS', dataRM: action.data };
    case types.GET_ZM_AREA_LEVEL_SUMMARY_INITIATED:
      return { ...state, statusZM: 'PENDING' };
    case types.GET_ZM_AREA_LEVEL_SUMMARY_FAILURE:
      return { ...state, statusZM: 'FAILURE', error: action.error };
    case types.GET_ZM_AREA_LEVEL_SUMMARY_SUCCESS:
      return { ...state, statusZM: 'SUCCESS', dataZM: action.data };
    default:
      return state;
  }
}

export default areaLevelSummary;
