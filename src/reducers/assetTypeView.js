import initialState from './initialState';
import * as types from '../actions/actionTypes';

function assetTypeView(state = initialState.assetTypeView, action) {
  switch (action.type) {
    case types.GET_ASSET_VIEW_INITIATED:
      return { ...state, status: 'PENDING' };
    case types.GET_ASSET_VIEW_SUCCESS:
      return { ...state, status: 'SUCCESS', ...action.data };
    case types.GET_ASSET_VIEW_FAILURE:
      return { ...state, error: action.error, status: 'FAILURE' };
    case types.GET_RM_ASSET_VIEW_INITIATED:
      return { ...state, rmStatus: 'PENDING' };
    case types.GET_RM_ASSET_VIEW_SUCCESS:
      return { ...state, rmStatus: 'SUCCESS', ...action.data };
    case types.GET_RM_ASSET_VIEW_FAILURE:
      return { ...state, error: action.error, rmStatus: 'FAILURE' };
    case types.GET_ZM_ASSET_VIEW_INITIATED:
      return { ...state, zmStatus: 'PENDING' };
    case types.GET_ZM_ASSET_VIEW_SUCCESS:
      return { ...state, zmStatus: 'SUCCESS', ...action.data };
    case types.GET_ZM_ASSET_VIEW_FAILURE:
      return { ...state, error: action.error, zmStatus: 'FAILURE' };
    default:
      return state;
  }
}

export default assetTypeView;
