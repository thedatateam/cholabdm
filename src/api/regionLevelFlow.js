import {
    endpoint
} from '../config';
import auth from '../modules/auth';
const host = endpoint.host;
// const host = node.host;
class regionManagerFlowAPI {
    static getRegionLevelSummary(region,period,role) {
        return fetch(`${host}/api/cholaBDM/getRegionLevelSummary/?Region=${region}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
    static getGraphData(region,period) {
        return fetch(`${host}/api/cholaBDM/getCustomGraphData/?Type1=Region&Value1=${region}&Period=${period}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
       static regionProductOutput(area,period,role) {
        return fetch(`${host}/api/cholaBDM/RegionProductLevelOutput/?Region=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);          
    }
        static updateRegionProductLevelOutput(updatedData) {
        return fetch(`${host}/api/cholaBDM/RegionProductLevelInput`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'PUT',
                body: JSON.stringify(updatedData)
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }

    static getNewAssetView(region,period,role) {
        return fetch(`${host}/api/cholaBDM/getNewAssetRMView/?Region=${region}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static getUsedAssetView(region,period,role) {
        return fetch(`${host}/api/cholaBDM/getUsedAssetRMView/?Region=${region}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static getShubhAssetView(region,period,role) {
        return fetch(`${host}/api/cholaBDM/getShubhAssetRMView/?Region=${region}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static getAllAssetView(region,period,role) {
        return fetch(`${host}/api/cholaBDM/RMSummary/?Region=${region}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static getRegionFinalizedRate(area, offset, limit) {
        return fetch(`${host}/api/cholaBDM/getFinalizedRate?Type=Region&Name=${area}&Offset=${offset}&Limit=${limit}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static submitRegionResponse(region, period, role) {
        return fetch(`${host}/api/cholaBDM/approveBDM/?Type=Region&Name=${region}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }

    static reworkRegionResponse(region, period, role) {
        return fetch(`${host}/api/cholaBDM/rejectBDM/?Type=Region&Name=${region}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }

    static reworkRegionCommentResponse(region, period, role, json) {
        return fetch(`${host}/api/cholaBDM/rejectBDM/?Type=Region&Name=${region}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'PUT',
            body: JSON.stringify(json)
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }

    static regionDetailedView(region, period, role, segment, offset, limit) {
        return fetch(`${host}/api/cholaBDM/RegionDetailedViewOutput/?Region=${region}&Segment=${segment}&Period=${period}&Role=${role}&Offset=${offset}&Limit=${limit}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }
}

export default regionManagerFlowAPI;
