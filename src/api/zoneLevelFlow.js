import {
    endpoint
} from '../config';
import auth from '../modules/auth';
const host = endpoint.host;
// const host = node.host;
class zoneManagerFlowAPI {
    static getZoneLevelSummary(zone,period,role) {
        return fetch(`${host}/api/cholaBDM/getZoneLevelSummary/?Zone=${zone}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
    static getGraphData(zone,period) {
        return fetch(`${host}/api/cholaBDM/getCustomGraphData/?Type1=Zone&Value1=${zone}&Period=${period}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
       static zoneProductOutput(zone,period,role) {
        return fetch(`${host}/api/cholaBDM/ZoneProductLevelOutput/?Zone=${zone}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);          
    }
        static updateZoneProductLevelOutput(updatedData) {
        return fetch(`${host}/api/cholaBDM/ZoneProductLevelInput`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'PUT',
                body: JSON.stringify(updatedData)
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }


    static getZoneFinalizedRate(zone,offset) {
        return fetch(`${host}/api/cholaBDM/getFinalizedRate?Type=Zone&Name=${zone}&Offset=${offset}&Limit=1000`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }
    static getNewAssetView(zone,period,role) {
        return fetch(`${host}/api/cholaBDM/getNewAssetZMView/?Zone=${zone}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static getUsedAssetView(zone,period,role) {
        return fetch(`${host}/api/cholaBDM/getUsedAssetZMView/?Zone=${zone}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static getShubhAssetView(zone,period,role) {
        return fetch(`${host}/api/cholaBDM/getShubhAssetZMView/?Zone=${zone}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static getAllAssetView(zone,period,role) {
        return fetch(`${host}/api/cholaBDM/ZMSummary/?Zone=${zone}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
            
    }

    static zoneDetailedView(zone, period, role, segment, offset, limit) {
        return fetch(`${host}/api/cholaBDM/ZoneDetailedViewOutput/?Zone=${zone}&Segment=${segment}&Period=${period}&Role=${role}&Offset=${offset}&Limit=${limit}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }

    static zoneRegionSummary(zone, period, role) {
        return fetch(`${host}/api/cholaBDM/ZM2Summary/?Zone=${zone}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }

    static zoneAreaSummary(zone, period, role) {
        return fetch(`${host}/api/cholaBDM/ZM3Summary/?Zone=${zone}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }

    static submitForApproval(zone, period, role) {
        return fetch(`${host}/api/cholaBDM/approveBDM/?Type=Zone&Name=${zone}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }

}

export default zoneManagerFlowAPI;
