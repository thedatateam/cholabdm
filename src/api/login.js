import {
    endpoint
} from '../config';
import auth from '../modules/auth';
const host = endpoint.host;
// const host = node.host;
class authApi {
    static login(token) {
        return fetch(`${host}/api/authenticate/?token=${token}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
    static logout() {
        return fetch(`${host}/user/logout`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'GET'
            })
            .then((response) => auth.verifyResponse(response))
            .then((json) => json)
            .catch((err) => err);
    }
    static forgot(area,period,role) {
        return fetch(``, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
            })
            .then((response) => auth.verifyResponse(response))
            .then((json) => json)
            .catch((err) => err);
    }
    static token(authToken) {
        return fetch(`${host}/auth/${authToken}/reset`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'GET'
            })
            .then((response) => auth.verifyResponse(response))
            .then((json) => json)
            .catch((err) => err);
    }
    static reset(area,period, role) {
        return fetch(`${host}/api/cholaBDM/getAreaLevelSummary/?Area=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`

            
                },
                method: 'GET',            })
            .then((response) => auth.verifyResponse(response))
            .then((json) => json)
            .catch((err) => err);
    }
    static change(userParams) {
        return fetch(`${host}/auth/change`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('_userId')}`
                },
                method: 'POST',
                body: JSON.stringify(userParams)
            })
            .then((response) => auth.verifyResponse(response))
            .then((json) => json)
            .catch((err) => err);
    }
}

export default authApi;