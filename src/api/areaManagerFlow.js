import {
    endpoint
} from '../config';
import auth from '../modules/auth';
const host = endpoint.host;
// const host = node.host;

class areaManagerFlowAPI {
    static getAreaLevelSummary(area,period,role) {
        return fetch(`${host}/api/cholaBDM/getAreaLevelSummary/?Area=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
    static getFinalizedRate(area,offset) {
        return fetch(`${host}/api/cholaBDM/getFinalizedRate?Type=Area&Name=${area}&Offset=${offset}&Limit=1000`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
    static productLevelOutput(area,period,role) {
        return fetch(`${host}/api/cholaBDM/ProductLevelOutput/?Area=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);          
    }
    static getCustomerSegmentGraphData(type,area,segment,period) {
        return fetch(`${host}/api/cholaBDM/getMakeGraphData/?Type1=${type}&Value1=${area}&Segment=${segment}&Period=${period}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
    static getCustomerCategoryGraphData(type,area,segment,period) {
        return fetch(`${host}/api/cholaBDM/getCategoryGraphData/?Type1=${type}&Value1=${area}&Segment=${segment}&Period=${period}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }

    static getBDMTemplateStatus(area,period) {
        return fetch(`${host}/api/cholaBDM/getBDMTemplateStatus?Type=Area&Name=${area}&Period=${period}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
       
    }

    static updateProductLevelOutput(updatedData) {
        return fetch(`${host}/api/cholaBDM/ProductLevelInput`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'PUT',
                body: JSON.stringify(updatedData)
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }
    static getNewAssetView(area,period,role) {
        return fetch(`${host}/api/cholaBDM/getNewAssetView/?Area=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }

    static getUsedAssetView(area,period,role) {
        return fetch(`${host}/api/cholaBDM/getUsedAssetView/?Area=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }

    static getShubhAssetView(area,period,role) {
        return fetch(`${host}/api/cholaBDM/getShubhAssetView/?Area=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }

    static getAllAssetView(area,period,role) {
        return fetch(`${host}/api/cholaBDM/?Area=${area}&Period=${period}&Role=${role}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }

    static submitForApproval(area, period, role) {
        return fetch(`${host}/api/cholaBDM/submitBDM/?Area=${area}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }
            
    static detailedView(area, period, role, segment, offset, limit) {
        return fetch(`${host}/api/cholaBDM/detailedView/?Area=${area}&Segment=${segment}&Period=${period}&Role=${role}&Offset=${offset}&Limit=${limit}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }
    static getAllDropDown(type) {
        return fetch(`${host}/api/cholaBDM/cholaBDM/getAllDropDown/?Type=Segment`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }
    static searchFinalizedRate(flow,area,key,value) {
        return fetch(`${host}/api/cholaBDM/searchFinalizedRate?${flow}=${area}&${key}=${value}&Limit=10&Offset=0`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                method: 'GET',
            })
            .then((response) => auth.verifyResponse(response)) 
            .then((json) => json)
            .catch((err) => err);
           
    }

}

export default areaManagerFlowAPI;