import {
    endpoint
} from '../config';
import auth from '../modules/auth';
const host = endpoint.host;
class areaLevelSummaryApi {
    static getRMAreaLevelSummary(region, period, role) {
        return fetch(`${host}/api/cholaBDM/RM2Summary/?Region=${region}&Period=${period}&Role=${role}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('token')}`
            },
            method: 'GET',
        })
        .then((response) => auth.verifyResponse(response)) 
        .then((json) => json)
        .catch((err) => err);
    }
}

export default areaLevelSummaryApi;