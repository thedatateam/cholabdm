import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from "redux";
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Backdrop from '@material-ui/core/Backdrop';
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress';
import FormHelperText from "@material-ui/core/FormHelperText";
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import {login,reset} from './../../actions/login'
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
const parseUrl = require("parse-url")


const useStyles = makeStyles(theme => ({
  formControl: {
    minWidth: 520,
    marginLeft:'20px',
    marginRight:'20px',
    marginBottom:'30px',
    marginTop:'30px'


  },
  formControl1: {
    minWidth: 520,
    marginLeft:'20px',
    marginRight:'20px',
    marginTop:'30px',
    marginBottom:'30px',

  },
  
}));


const Login = ({ user, login,reset }) => {
  const classes = useStyles();
  document.title = "Login | CholaBDM";
  const [error, setError] = React.useState({
    email: {
      msg:'',
      status: false
    },
    password: {
      msg: '',
      status: false
    }
  });
  const [loginPending, setLoginPending] = React.useState(false);

  const [selctionText, setSelctionText] = React.useState('');
  const [region, setRegionData] = React.useState([]);
  const [zone, setZoneData] = React.useState([]);
  const [area, setAreaData] = React.useState([]);
  const [valueText,setValueText] = React.useState('');
  const [listData, setListData] = React.useState([]);
  const history = useHistory();
  const dispatch = useDispatch();


  React.useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      let url= window.location.href;
      var tokendata=parseUrl(url);
  
      if(tokendata.query && tokendata.query.token ){
        let token= tokendata.query.token;
        // let token='';
      login(token);
      }
    }
    return () => { isMounted = false };
  }, [])
  let regions = useSelector((state) =>state.user && state.user.response? state.user.response.region:[]);
  if(user && user.loginSuccess===true) {
setRegionData(user.response.region);
setZoneData(user.response.zone);
setAreaData(user.response.area)
localStorage.setItem('token',user.response.token)
user.loginSuccess=false;


  }
 
 
  const handleChange = (event) => {
    const name = event.target.name;
    const value= event.target.value;
    if(value === "Region"){
      setListData(region)
      localStorage.setItem('role','RBM');

      }
     else if(value === 'Zone'){
      setListData(zone)
      localStorage.setItem('role','ZBM');

  
        }
       else if(value === 'Area'){
        setListData(area)
        localStorage.setItem('role','ABM');


       }
      
    setSelctionText(event.target.value);
    localStorage.setItem('type',event.target.value);


  };
  const handleValueChange = (event) => {
    const name = event.target.name;
    const value= event.target.value;
    
      
    setValueText(event.target.value);
    localStorage.setItem('typeValue',event.target.value);


  };
  const handleNext = () => {
    if(selctionText==='Area'){
    window.location = '/areaManagerFlow';  
    
} else if (selctionText === 'Region') {
  window.location = '/regionLevelFlow';
}
else if (selctionText === 'Zone') {
  window.location = '/zoneLevelFlow';
}

   };
   const handleAdminLogin = () => {
    window.location = '/adminLogin';  
    
}
  return (
    <div >
    <Dialog  aria-labelledby="simple-dialog-title" open={true} style={{ margin:'auto'}}>
      <DialogTitle id="simple-dialog-title" style={{marginTop:'30px'}}>Select the Options to Proceed
</DialogTitle>
<Typography variant="h6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name</Typography>

<FormControl variant="outlined" className={classes.formControl}>
        <Select
          value={selctionText}
          onChange={handleChange}
        >
          <MenuItem value={'Area'}> Area</MenuItem>
          <MenuItem value={'Region'}>Region</MenuItem>
          <MenuItem value={'Zone'}>Zone</MenuItem>
        </Select>
      </FormControl>
      <Typography variant="h6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Value</Typography>
      <FormControl variant="outlined" className={classes.formControl1}>
        <Select
          value={valueText}
          onChange={handleValueChange}
        >
        
        {listData.map(data => 
                       <MenuItem key={data} value={data}>{data}</MenuItem>
                    )}
        </Select>
      </FormControl>
      <Button variant="contained"    
      color="primary"
      onClick={handleNext}
      style={{  textTransform: 'none',float:'right', marginRight:'10px',alignSelf:'center',width:'100px',margin:"20px"}} 
                            >
                          
                           Continue</Button>
                           <Link   onClick={handleAdminLogin} style={{padding: 20}}> Or click here for Admin Login</Link>
    </Dialog>
        <Backdrop className={classes.backdrop} open={user && user.loginSuccess===true}>
          <CircularProgress color="inherit" />Authenticating
        </Backdrop>
    </div>
  );
}

const mapStateToProps = state => ({
  user: state.user
});




const mapDispatchToProps = { login ,reset};

export default connect(mapStateToProps, mapDispatchToProps)(Login);