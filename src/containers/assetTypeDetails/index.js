import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core';
import cn from 'classnames';
import CholaLogo from '../../assets/images/cholaLogo.png';
import {
  getAssetDetailedView,
  getRMAssetDetailedView,
  getZMAssetDetailedView,
} from './../../actions/assetDetailedView';
import BookROTA from '../../components/AssetDetailedTabs/BookROTA';
import MarginalROTA from '../../components/AssetDetailedTabs/MarginalROTA';
import Summary from '../../components/AssetDetailedTabs/Summary';
import Pagination from '../../components/Pagination';

const useStyles = makeStyles({
  tabHead: {
    backgroundColor: '#f6f6f6',
    marginBottom: 20,
  },
  colorBox: {
    display: 'flex',
    alignItems: 'center',
    gap: 10,
  },
  boxList: {
    paddingBottom: 20,
    paddingLeft: 20,
  },
  box: {
    height: 20,
    width: 20,
    border: '1px solid #000',
  },
  blue: {
    backgroundColor: '#abd6dfff',
  },
  purple: {
    backgroundColor: '#7F6084',
  },
  orange: {
    backgroundColor: '#ff8533',
  },
  yellow: {
    backgroundColor: '#fff133',
  },
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <>{children}</>}
    </div>
  );
}

const AssetTypeDetails = () => {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const assetDetailedView = useSelector((state) => state.assetDetailedView);
  const [value, setValue] = useState(0);
  const [page, setPage] = useState(1);
  const [limit] = useState(1000);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function handleSummaryPage() {
    const type = localStorage.getItem('type');
    if (type === 'Area') {
      history.push('/areaManagerFlow');
    } else if (type === 'Region') {
      history.push('/regionLevelFlow');
    } else if (type === 'Zone') {
      history.push('/zoneLevelFlow');
    }
  }

  React.useEffect(() => {
    const segment = localStorage.getItem('segment');
    const type = localStorage.getItem('type');
    if (type === 'Area') {
      dispatch(getAssetDetailedView(segment));
    } else if (type === 'Region') {
      dispatch(getRMAssetDetailedView(segment));
    } else if (type === 'Zone') {
      dispatch(getZMAssetDetailedView(segment));
    }
  }, []);

  const onPageChange = (page) => {
    const segment = localStorage.getItem('segment');
    const type = localStorage.getItem('type');
    const offset = (page - 1) * limit;
    if (type === 'Area') {
      dispatch(getAssetDetailedView(segment, offset));
    } else if (type === 'Region') {
      dispatch(getRMAssetDetailedView(segment, offset));
    } else if (type === 'Zone') {
      dispatch(getZMAssetDetailedView(segment, offset));
    }
    setPage(page);
  };

  return (
    <>
      <Grid container>
        <Grid item xs={3}>
          <img
            src={CholaLogo}
            style={{ margin: '10px', width: '150px', height: '100px' }}
          />
        </Grid>
        <Grid item xs={6} style={{ textAlign: 'center', marginTop: '30px' }}>
          <Typography variant="h6" style={{ fontWeight: '200' }}>
            Detailed View
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Button
            variant="contained"
            onClick={handleSummaryPage}
            style={{
              textTransform: 'none',
              backgroundColor: '#0275d8',
              margin: '20px 20px 0',
              color: 'white',
            }}
          >
            Back To Summary{' '}
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Paper elevation={3} style={{ margin: 20 }}>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={handleChange}
              aria-label="disabled tabs example"
              className={classes.tabHead}
            >
              <Tab label="Book ROTA" />
              <Tab label="Marginal ROTA" />
              <Tab label="Summary" />
            </Tabs>

            <Grid
              container
              direction="row"
              alignItems="center"
              className={classes.boxList}
              spacing={2}
            >
              <Grid item className={classes.colorBox}>
                <div className={cn(classes.box, classes.blue)}></div>Less than
                Previous Actual_IRR
              </Grid>
              <Grid item className={classes.colorBox}>
                <div className={cn(classes.box, classes.purple)}></div>Less than
                Previous Planned_IRR
              </Grid>
              <Grid item className={classes.colorBox}>
                <div className={cn(classes.box, classes.orange)}></div>Less than
                Actual ROTA
              </Grid>
              <Grid item className={classes.colorBox}>
                <div className={cn(classes.box, classes.yellow)}></div>Less than
                Extrapolated Value
              </Grid>
            </Grid>

            {assetDetailedView.status === 'PENDING' && (
              <Grid container spacing={2} style={{ padding: 20 }}>
                <Grid item>Loading Details Data...</Grid>
              </Grid>
            )}

            {assetDetailedView.status === 'SUCCESS' && (
              <>
                <TabPanel value={value} index={0}>
                  <BookROTA details={assetDetailedView.data.overalldata} />
                </TabPanel>
                <TabPanel value={value} index={1}>
                  <MarginalROTA details={assetDetailedView.data.overalldata} />
                </TabPanel>
                <TabPanel value={value} index={2}>
                  <Summary details={assetDetailedView.data.overalldata} />
                </TabPanel>
              </>
            )}
            <Pagination
              pageChange={onPageChange}
              page={page}
              count={assetDetailedView.data?.totalPages}
            />
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AssetTypeDetails;
