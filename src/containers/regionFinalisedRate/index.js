import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { getRegionFinalizedRate } from './../../actions/regionFinalisedRate';
import { searchFinalizedRate } from './../../actions/areaManagerFlow';
import CholaLogo from '../../assets/images/cholaLogo.png';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/SearchOutlined';
import Pagination from '../../components/Pagination';
import { ColorLensTwoTone } from '@material-ui/icons';

const RegionFinalisedRate = () => {
  let gridApi;
  let gridColumnApi;
  const history = useHistory();
  const dispatch = useDispatch();
  const regionFinalisedRate = useSelector((state) => state.regionFinalisedRate);
  const searchfinalizedRateData = useSelector(
    (state) => state.searchfinalizedRateData
  );
  const areaManager = useSelector((state) => state.areaManager);
  const [search, setSearch] = useState();
  const [searchfinalizedRateRegionData, setSearchfinalizedRateData] = useState();
  setSearchfinalizedRateData
  const [searchedText, setSearchedText] = useState('');
  const [selectedValue, setSelectedValue] = useState('');
  const [showField, setShowField] = useState(false);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [limit] = useState(1000);

  const columnDefs = [
    {
      headerName: ' AREA',
      field: 'Area',
      suppressMenu: true,
      tooltipValueGetter: (params) => params.value,
    },
    {
      headerName: 'REGION',
      field: 'Region',
      sortingOrder: ['asc'],
      suppressMenu: true,
      tooltipValueGetter: (params) => params.value,
    },
    {
      headerName: 'ZONE',
      field: 'Zone',
      suppressMenu: true,
      tooltipValueGetter: (params) => params.value,
    },
    {
      headerName: 'PERIOD',
      field: 'Period',
      suppressMenu: true,
      tooltipValueGetter: (params) => params.value,
    },
    { headerName: 'YEAR', field: 'Year' },
    {
      headerName: 'SEGMENT',
      field: 'Segment',
      tooltipValueGetter: (params) => params.value,
    },
    { headerName: 'NEW/USED', field: 'NewOrUsed' },
    { headerName: 'ASSET DESC', field: 'Assetdesc' },
    { headerName: 'MAKE', field: 'Make' },
    { headerName: 'CATEGORY ', field: 'Category' },
    {
      headerName: 'PLANNED IRR(LAST MONTH)',
      field: 'Last_Month_Planed_IRR',
    },
    { headerName: 'PLANNED IRR(%)', field: 'Planned_IRR' },
    { headerName: 'LAST EDITED BY', field: 'Last_Edited_By' },
    { headerName: 'LAST APPROVED BY', field: 'Last_Approved_By' },
  ];

  const gridOptions = {
    defaultColDef: {
      resizable: true,
      sortable: true,
    },
    cacheBlockSize: 10,
    localeText: { noRowsToShow: 'No Data Found' },
  };

  const dropDown = (event) => {
    const { value } = event.target;
    setShowField(true);
    setSelectedValue(value);
  };
 const handleSearch = prop => event => {  
    let area = localStorage.getItem('typeValue');
     dispatch(searchFinalizedRate('Region',area,selectedValue,searchedText))
  };
 const handleTextChange = ( event) => {
  const { value } = event.target;

  setSearchedText(value);   
  };
  React.useEffect(() => {
    dispatch(getRegionFinalizedRate());
  }, []);

  useEffect(() => {
    if (regionFinalisedRate.status === 'SUCCESS') {
      setTotalPages(regionFinalisedRate.data.totalPages);
    }
  }, [regionFinalisedRate]);

  if (areaManager && areaManager.searchFinalizedRateSuccess === true) {
    if (areaManager.response && areaManager.response.SearchFinalizedRate)
      setSearchfinalizedRateData(areaManager.response.SearchFinalizedRate);
    areaManager.searchFinalizedRateSuccess = false;
  }

  function handleSummaryPage() {
    history.push('/regionLevelFlow');
  }

  function onGridReady(params) {
    gridApi = params.api;
    gridColumnApi = params.columnApi;
    gridApi.sizeColumnsToFit();
  }

  function onPageChange(value) {
    const offset = (value - 1) * limit;
    dispatch(getRegionFinalizedRate(offset, limit));
    setPage(value);
  }


  return (
    <>
      <Grid container>
        <Grid item xs={3}>
          <img
            src={CholaLogo}
            style={{ margin: '10px', width: '150px', height: '100px' }}
          />
        </Grid>
        <Grid item xs={6} style={{ textAlign: 'center', marginTop: '30px' }}>
          <Typography variant="h6" style={{ fontWeight: '200' }}>
            Finalized Rate
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid container justifyContent="space-between">
            <Grid item>
              <Button
                variant="contained"
                onClick={handleSummaryPage}
                style={{
                  textTransform: 'none',
                  backgroundColor: '#0275d8',
                  margin: '20px 20px 0',
                  color: 'white',
                }}
              >
                Back To Summary{' '}
              </Button>
            </Grid>
            <Grid item>
              <Grid container style={{ gap: 20, padding: '0 20px' }}>
                <Grid item>
                <FormControl style={{ width: 250 }}>
            <InputLabel id="assetType">Enter Value</InputLabel>
            <Select
              labelId="EnterValue"
              id="type"
              defaultValue={''}
              name="value"
              value={selectedValue}
              onChange={dropDown}
            >     <MenuItem value={'Area'}>AREA</MenuItem>
                  <MenuItem value={'Region'}>REGION</MenuItem>
                  <MenuItem value={'Zone'}>ZONE</MenuItem>
                  <MenuItem value={'Period'}>PERIOD</MenuItem>
                  <MenuItem value={'Year'}>YEAR</MenuItem>
                  <MenuItem value={'Segment'}>SEGMENT</MenuItem>
                  <MenuItem value={'NewOrUsed'}>NEW/USED</MenuItem>
                  <MenuItem value={'Assetdesc'}>ASSET DESC</MenuItem>
                  <MenuItem value={'Category'}>Category </MenuItem>
                  <MenuItem value={'Make'}>MAKE</MenuItem>
                  <MenuItem value={'Last_Edited_By'}>LAST EDITED BY</MenuItem>
                  <MenuItem value={'Last_Approved_By'}>LAST APPROVED BY</MenuItem>
            </Select>
            </FormControl>&nbsp;&nbsp;{showField ? (
          <TextField
          id="standard-helperText"
          type={'text'}
          onChange={(e) => handleTextChange(e)}
          label="Search a Value.."                      
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="Search a user"
                  style={{ color: '#007bc9' }}
                  value={search}
                  onClick={handleSearch('search')}
                  autoFocus
                ><SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        /> 
        ) : null}             

                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Paper elevation={3} style={{ margin: 20 }}>
            {regionFinalisedRate.status === 'PENDING' && (
              <div
                style={{
                  height: '450px',
                  width: '100%',
                  display: 'flex',
                  placeItems: 'center',
                  justifyContent: 'center',
                }}
              >
                Loading...
              </div>
            )}
            {regionFinalisedRate.status === 'SUCCESS' && (
              <div
                className="ag-theme-alpine"
                style={{
                  height: '450px',
                  width: '100%',
                }}
              >
                <AgGridReact
                  rowData={
                    (searchfinalizedRateRegionData && searchfinalizedRateRegionData.length)?searchfinalizedRateRegionData:(searchfinalizedRateData && searchFinalizedRateData.length
                      ? searchFinalizedRateData
                      : regionFinalisedRate.data?.FinalizedRate)
                  }
                  columnDefs={columnDefs}
                  onGridReady={onGridReady}
                  pagination={false}
                  gridOptions={gridOptions}
                ></AgGridReact>
              </div>
            )}
            <Pagination
              pageChange={onPageChange}
              page={page}
              count={totalPages}
            />
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
export default RegionFinalisedRate;
