import React, {Component} from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';

const theme = createTheme({
  props: {
    // Name of the component ⚛️
    MuiButtonBase: {
      // The default props to change
      disableRipple: true, // No more ripple, on the whole application 💣!
    },
  },
  typography: {
    "fontFamily": "\"Raleway\", sans-serif",
    "fontSize": 14,
    "fontWeightLight": 300,
    "fontWeightRegular": 400,
    "fontWeightMedium": 500
   }
});

import Login from './login';
import AdminLogin from './adminLogin';
import forgetPassword from './forgetPassword';
import resetPassword from './resetPassword';
import AreaManagerFlow from './areaManagerFlow'
import FinalizedRate  from '../components/FinalizedRate'
import ProductLevelInput  from '../components/ProductLevelInput'
import RegionProductLevelInput from '../components/RegionProductLevelInput';
import AssetTypeDetails from './assetTypeDetails';
import RegionLevelFlow from './regionLevelFlow';
import zoneLevelFlow from './zoneLevelFlow';
import RegionFinalisedRate from './regionFinalisedRate';
import ZoneFinalizedRate from '../components/ZoneFinalizedRate';
import ZoneProductLevelInput from '../components/ZoneProductLevelInput';
import AdminDashboard from './adminDashboard';

class CholaBDM extends Component{
    render() {
    //   const PrivateRoute = ({ component: Component, ...rest }) => (
    //     <Route {...rest} render={(props) => (
    //       true
    //         ? <Component {...props} />
    //         : <Redirect to='/mdmconfig/login' />
    //     )} />
    //   )
      const PublicRoute =  ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
          false
            ? 
              <Redirect to='/login' />
            :
            <Component {...props} />
        )} />
      )
      return(
        <ThemeProvider theme={theme}>
          <BrowserRouter>
              <Switch>
                  <Route exact path="/">
                      <Redirect from='/' to='/login'/>
                  </Route>
                  <PublicRoute exact path="/login" component={Login} />
                  <PublicRoute exact path="/adminLogin" component={AdminLogin} />
                  <PublicRoute exact path="/forget" component={forgetPassword} />
                  <PublicRoute exact path="/reset" component={resetPassword} />
                  <PublicRoute exact path="/areaManagerFlow" component={AreaManagerFlow} />
                  <PublicRoute exact path="/regionLevelFlow" component={RegionLevelFlow} />
                  <PublicRoute exact path="/zoneLevelFlow" component={zoneLevelFlow} />
                  <PublicRoute exact path="/FinalizedRate" component={FinalizedRate } />
                  <PublicRoute exact path="/RegionFinalizedRate" component={RegionFinalisedRate} />
                  <PublicRoute exact path="/ProductLevelInput" component={ProductLevelInput } />
                  <PublicRoute exact path="/RegionProductLevelInput" component={RegionProductLevelInput } />
                  <PublicRoute path="/mainSheet" component={AssetTypeDetails} />
                  <PublicRoute exact path="/ZoneProductLevelInput" component={ZoneProductLevelInput } />
                  <PublicRoute exact path="/ZoneFinalizedRate" component={ZoneFinalizedRate } />
                  <PublicRoute exact path="/admin" component={AdminDashboard} />



              </Switch>
          </BrowserRouter>
        </ThemeProvider>
      );
    }
  }
  export default CholaBDM;