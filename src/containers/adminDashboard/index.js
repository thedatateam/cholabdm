import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core';
import CholaLogo from '../../assets/images/cholaLogo.png';
import LoadDataTab from '../../components/LoadDataTab';
import ModifyBranchStatus from '../../components/ModifyBranchStatus';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  tabHead: {
    backgroundColor: '#f6f6f6',
    marginBottom: 20,
  },
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <>{children}</>}
    </div>
  );
}
const AdminDashboard = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const history = useHistory();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleLogout = () => {
    alert('Successfully logged out');
    history.push('/')
  }

  return (
    <>
      <Grid container justify="space-between">
        <Grid item>
          <img
            src={CholaLogo}
            style={{ margin: '10px', width: '150px', height: '100px' }}
          />
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            style={{
              textTransform: 'none',
              backgroundColor: '#0275d8',
              margin: '20px 20px 0',
              color: 'white',
            }}
            onClick={handleLogout}
          >
            Logout{' '}
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Paper elevation={3} style={{ margin: 20 }}>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={handleChange}
              aria-label="disabled tabs example"
              className={classes.tabHead}
            >
              <Tab label="Load Data" />
              <Tab label="Modify Branch Status" />
            </Tabs>

            <TabPanel value={value} index={0}>
              <LoadDataTab />
            </TabPanel>
            <TabPanel value={value} index={1}>
<ModifyBranchStatus />           
 </TabPanel>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AdminDashboard;
