import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import Card from '@material-ui/core/Card';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
// import { addCount } from "./store/counter/actions";

const useStyles = makeStyles(theme => ({
  loginFullPageContainer: {
    top: 0,
    left: 0,
    fontFamily: 'Raleway',
    [theme.breakpoints.up('sm')]: {
      background: 'no-repeat center center fixed',
      "-webkit-background-size": "cover",
      "-moz-background-size": "cover",
      "-o-background-size": "cover",
      backgroundSize: "cover",
      /* Set rules to fill background */
      minHeight: "100%",
      minWidth: 1024,    
      /* Set up proportionate scaling */
      width: "100%",
      height: "auto",    
      /* Set up positioning */
      position: "fixed"
    }
  },
  card: {
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: 'auto',
      overflow: 'auto',
      border: 'none'
    },
    [theme.breakpoints.up('sm')]: {
      width: 750,
      height: 450,
      position: "absolute",
      left: "50%",
      top: "50%",
      transform: "translate(-50%, -50%)",
      borderRadius: 25
    }
  },
  bgColor: {
    background: '#287bfa',
    height: 450,
    [theme.breakpoints.down('sm')]: {
      display:'none'
    }
  },
  width50: {
    width: "50%",
    float: "left",
    [theme.breakpoints.down('sm')]: {
      width: "100%",
      float: "none"
    }
  },
  introTextContainer: {
    marginTop: '10%',
    color: '#ffffff',
    paddingLeft: 60
  },
  introTextHeading: {
    fontWeight: 'bold'
  },
  introTextHeadingUnderline: {
    width: 100,
    height: 3,
    borderBottom: "3px white solid"
  },
  introTextHeadingText: {
    paddingTop: 20,
    width: "80%",
    fontSize: 13,
    paddingBottom: 20
  },
  introKnowMoreButton: {
    color: '#ffffff',
    border: '1px solid rgba(255, 255, 255, 0.5)',
    borderRadius: 25,
    textTransform: 'capitalize',
    outline: 'none',
    fontSize: 12
  },
  signInContainer: {
    fontFamily: 'Raleway'
  },
  signInTextHeading: {
    marginTop: "25%",
    width: "100%",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20
  },
  signInUnderline: {
    height: 10,
    borderBottom: "3px #287bfa solid",
    width: 30,
    marginRight: "auto",
    marginLeft: "auto"
  },
  signInField: {
    width: '80%'
  },
  signInDataFields: {
    paddingLeft: '10%',
    paddingTop: '7%'
  },
  loginSubmitButton: {
    color: '#ffffff',
    border: '1px solid #287bfa',
    borderRadius: 25,
    textTransform: 'capitalize',
    outline: 'none',
    fontSize: 12,
    background: '#287bfa',
    height: 40,
    width: 100,
    '&:focus': {
      border: '1px solid #164b9c',
      background: '#164b9c'
    },
    '&:hover': {
      border: '1px solid #164b9c',
      background: '#164b9c'
    },
    '&:active': {
      border: '1px solid #164b9c',
      background: '#164b9c'
    }
  },
  loginButtonContainer:{
    width: '100%',
    paddingTop: '8%',
    paddingLeft: '36%'
  },
  forgetPasswordLink: {
    float: 'right',
    padding: 30,
    paddingTop: 10,
    fontSize: 12,
    color: '#287bfa',
    cursor: 'pointer',
    textDecoration: 'none'
  },
  mobileLogo: {
    [theme.breakpoints.down('sm')]: {
      display: 'initial'
    },
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  }
}));
const CustomTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'black',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#cccccc',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#cccccc',
      },
      '&:hover fieldset': {
        borderColor: '#cccccc',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#cccccc',
      },
    },
  },
})(TextField);

const Login = () => {
  document.title = "Reset Password | Bharat Petroleum";
  const classes = useStyles();
  // const [age, setAge] = React.useState('');

  // const inputLabel = React.useRef(null);
  // const [labelWidth, setLabelWidth] = React.useState(0);
  // React.useEffect(() => {
  //   setLabelWidth(inputLabel.current.offsetWidth);
  // }, []);

  // const handleChange = event => {
  //   setAge(event.target.value);
  // };
  return (
    <div className={classes.loginFullPageContainer}>
       <Card className={classes.card} variant="outlined">
          <div>
            <div className={classNames(classes.width50, classes.bgColor)}>
              <div className={classes.introTextContainer}>
                <div className={classes.introTextHeading}>
                  Welcome to BPCL
                  <div className={classes.introTextHeadingUnderline}></div>
                </div>
                <div className={classes.introTextHeadingText}>
                Bharat Petroleum Corporation Limited is a Government of India controlled Maharatna O&G company. We're India's 2<sup>nd</sup> largest downstream oil company and is ranked 275<sup>th</sup> on the Fortune list of the world's biggest corporations as of 2019.
                </div>
                <Button variant="outlined" color="primary" onClick={ () => {
                  window.open(
                      'https://www.bharatpetroleum.com/About-BPCL/Our-Journey.aspx',
                      '_blank'
                    );
                }} className={classes.introKnowMoreButton}>
                  Know More
                </Button>
              </div>
            </div>
            <div className={classes.width50}>
                <div className={classes.signInContainer}>
                    <div className={classes.signInText}>
                      <div className={classes.signInTextHeading}>
                        forget Password
                        <div className={classes.signInUnderline}></div>
                      </div>
                    </div>
                    <div className={classes.signInForm}>
                    <form className={classes.signInDataFields} noValidate autoComplete="off">
                      <CustomTextField className={classes.signInField} id="auth-login-password" label="Password" type="password" /> <br/><br/>
                      <CustomTextField className={classes.signInField} id="auth-login-confirm-password" label="Confirm Password" type="password" />
                       </form>
                    <div className={classes.loginButtonContainer}>
                        <Button  style={{textTransform: 'none'}}variant="outlined" color="primary" className={classes.loginSubmitButton}>
                          Reset
                        </Button>
                    </div>
                    <Link to="login?resetPassword" className={classes.forgetPasswordLink}>
                        Login
                    </Link>
                    </div>
                </div>
            </div>
          </div>
        </Card>
    </div>
  );
}

const mapStateToProps = state => ({
  login: state.login
});

// const mapDispatchToProps = { addCount };

export default connect(mapStateToProps)(Login);