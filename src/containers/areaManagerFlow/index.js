import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from "redux";
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Backdrop from '@material-ui/core/Backdrop';
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress';
import FormHelperText from "@material-ui/core/FormHelperText";
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { getAreaLevelSummary,getFinalizedRate,getAllDropDown,getBDMTemplateStatus,productLevelOutput } from './../../actions/areaManagerFlow'
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import AreaMAnagerFlow from '../../components/AreaManagerFlow';
import CholaLogo from '../../assets/images/cholaLogo.png';
import AssetTypeView from '../../components/AssetTypeView';
import GraphVisual from '../../components/GraphVisuals';
import SubmitForApproval from "../../components/SubmitForApproval";

const useStyles = makeStyles(theme => ({
  formControl: {
    minWidth: 520,
    marginLeft: '20px',
    marginRight: '20px',
    marginBottom: '30px',
    marginTop: '30px'


  },
  formControl1: {
    minWidth: 520,
    marginLeft: '20px',
    marginRight: '20px',
    marginTop: '30px',
    marginBottom: '30px',

  },

}));


const AreaManagerFlow = ({ areaManager,getBDMTemplateStatus,getAllDropDown, getAreaLevelSummary,getFinalizedRate ,productLevelOutput}) => {
  const classes = useStyles();
  document.title = "AreaManagerFlow | CholaBDM";
  const [error, setError] = React.useState({
    email: {
      msg: '',
      status: false
    },
    password: {
      msg: '',
      status: false
    }
  });
  const [editedTimes, setEditedTimes] = React.useState(0);
  const [areaManagerData, setAreaManagerData] = React.useState([]);
  const history = useHistory();
  const dispatch = useDispatch();
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];

  const d = new Date();
  var n = monthNames[d.getMonth()]
  var year = new Date().getFullYear().toString().substr(-2);
  let nextYear = parseInt(year) + 1;
  let role = localStorage.getItem('role');
  let area = localStorage.getItem('typeValue');
  let type = localStorage.getItem('type');

  React.useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      (getAreaLevelSummary(area, n, role));
      (getBDMTemplateStatus(area,n));
      // (getAllDropDown(type));
      const prodLevelEdits = JSON.parse(localStorage.getItem('ProdLevelEditStack'));
      if (prodLevelEdits) {
        const num = [...new Set(prodLevelEdits.map(item => item.Stackid))].length;
        setEditedTimes(num);
      }
    }
    return () => { isMounted = false };
  }, [])
  if (areaManager && areaManager.getAreaLevelSummarySuccess === true) {
    if (areaManager.response && areaManager.response.overalldata)
      setAreaManagerData(areaManager.response.overalldata[0])
    areaManager.getAreaLevelSummarySuccess = false;
  }
  const handleNext = () => {
    window.location = '/FinalizedRate';  
    (getFinalizedRate(area));
    
}
function handleBack() {
  history.push('/login');
}
const handleProductLevelInput = () => {
  (productLevelOutput(area, n, role));
  
  window.location = '/ProductLevelInput';  

  
}
  return (
    <div>

      <Grid container spacing={3}>
        <Grid item xs>
          <img src={CholaLogo} style={{ margin: '10px', width: '150px', height: '100px' }} />
        </Grid>
        <Grid item xs style={{ textAlign: 'center', marginTop: '30px' }}>
          <Typography variant='h5' style={{ textAlign: 'center', fontWeight: '400' }}>Area Level Summary</Typography>
          {area ? <Typography variant='h6' style={{ textAlign: 'center', fontWeight: '200' }}> {area}</Typography> : null}
        </Grid>
        <Grid item xs style={{ textAlign: 'right', marginTop: '50px', marginRight: '10px', fontWeight: 'bold' }}>
          Current Month : {n} FY {year}-{nextYear}
        </Grid>
      </Grid>
      <Button variant="contained"
          onClick={ handleBack}
          style={{ textTransform: 'none',backgroundColor: '#0275d8', margin: '10px', color: 'white' }}>
          Back </Button>
      <Paper elevation={3} style={{margin: 20, padding: 20}}>
        <AreaMAnagerFlow areaData={areaManagerData} />
        <Grid container justifyContent="flex-end">
          <Grid item>
        <Button variant="contained"
          onClick={handleProductLevelInput}
          style={{  textTransform: 'none',backgroundColor: '#0275d8', margin: '10px', color: 'white' }}>
           Product Level Input
      </Button>
          </Grid>
          <Grid item>
        <Button variant="contained"
          onClick={handleNext}
          style={{ textTransform: 'none',backgroundColor: '#0275d8', margin: '10px', color: 'white' }}>
          Finalized Rate
      </Button>
          </Grid>
        </Grid>
     
      </Paper>
      <Paper elevation={3} style={{margin: 20, padding: 20}}>
        <AssetTypeView />
        <SubmitForApproval />

        <p>
          Number of Rows Edited: {editedTimes} from Product Level View.
        </p>
      </Paper>

      <GraphVisual />
      
    </div>
  );
}

const mapStateToProps = state => ({
  areaManager: state.areaManager
});

const mapDispatchToProps = { getAreaLevelSummary ,getAllDropDown,getFinalizedRate,getBDMTemplateStatus,productLevelOutput};

export default connect(mapStateToProps, mapDispatchToProps)(AreaManagerFlow);