import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CholaLogo from '../../assets/images/cholaLogo.png';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import { getZoneLevelSummary } from './../../actions/zoneLevelFlow';
import ZoneManagerFlow from '../../components/ZoneManagerFlow';
import ZoneGraphVisuals from '../../components/ZoneGraphVisuals';
import AssetTypeView from '../../components/AssetTypeView';
import AreaLevelSummary from '../../components/AreaLevelSummary';
import ZoneRegionSummary from '../../components/ZoneRegionSummary';
import ZoneApproveButton from '../../components/ZoneApproveButton';

const ZoneLevelFlow = ({ zoneManager, getZoneLevelSummary }) => {
  document.title = 'ZoneLevelFlow | CholaBDM';
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  const [error, setError] = React.useState({
    email: {
      msg: '',
      status: false,
    },
    password: {
      msg: '',
      status: false,
    },
  });
  const history = useHistory();
  const [zoneManagerData, setZoneManagerData] = React.useState([]);
  const d = new Date();
  const n = monthNames[d.getMonth()];
  const zone = localStorage.getItem('typeValue');
  const year = new Date().getFullYear().toString().substr(-2);
  const nextYear = parseInt(year) + 1;
  let role = localStorage.getItem('role');
  let type = localStorage.getItem('type');
  React.useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      getZoneLevelSummary(zone, n, role);
    }
    return () => {
      isMounted = false;
    };
  }, []);
  if (zoneManager && zoneManager.getZoneLevelSummarySuccess === true) {
    if (zoneManager.response && zoneManager.response.overalldata)
      setZoneManagerData(zoneManager.response.overalldata[0]);
    zoneManager.getZoneLevelSummarySuccess = false;
  }
  const handleProductLevelInput = () => {
    // (productLevelOutput(area, n, role));

    window.location = '/ZoneProductLevelInput';
  };
  function handleBack() {
    history.push('/login');
  }
  const goToFinalisedRateView = () => {
    history.push('/ZoneFinalizedRate');
  };
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs>
          <img
            src={CholaLogo}
            style={{ margin: '10px', width: '150px', height: '100px' }}
          />
        </Grid>
        <Grid item xs style={{ textAlign: 'center', marginTop: '30px' }}>
          <Typography variant='h5' style={{ textAlign: 'center', fontWeight: '400' }}>Zone Level Summary</Typography>
          {zone ? <Typography variant='h6' style={{ textAlign: 'center', fontWeight: '200' }}> {zone}</Typography> : null}
        </Grid>
        <Grid
          item
          xs
          style={{
            textAlign: 'right',
            marginTop: '50px',
            marginRight: '10px',
            fontWeight: 'bold',
          }}
        >
          Current Month : {n} FY {year}-{nextYear}
        </Grid>
      </Grid>
      <Button
        variant="contained"
        onClick={handleBack}
        style={{
          textTransform: 'none',
          backgroundColor: '#0275d8',
          margin: '10px',
          color: 'white',
        }}
      >
        Back
      </Button>
      <Paper elevation={3} style={{ margin: 20, padding: 20 }}>
        <ZoneManagerFlow zoneData={zoneManagerData} />
        <Grid container justifyContent="flex-end">
          <Grid item>
            <Button
              variant="contained"
              onClick={handleProductLevelInput}
              style={{
                textTransform: 'none',
                backgroundColor: '#0275d8',
                margin: '10px',
                color: 'white',
              }}
            >
              Product Level Input
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              onClick={goToFinalisedRateView}
              style={{
                textTransform: 'none',
                backgroundColor: '#0275d8',
                margin: '10px',
                color: 'white',
              }}
            >
              Finalized Rate
            </Button>
          </Grid>
        </Grid>
      </Paper>

      <Paper elevation={3} style={{ margin: 20, padding: 20 }}>
        <AssetTypeView />
        <ZoneRegionSummary />
        <AreaLevelSummary />
        <ZoneApproveButton />
      </Paper>
      <ZoneGraphVisuals />
    </div>
  );
};

const mapStateToProps = (state) => ({
  zoneManager: state.zoneManager,
});

const mapDispatchToProps = { getZoneLevelSummary };

export default connect(mapStateToProps, mapDispatchToProps)(ZoneLevelFlow);
