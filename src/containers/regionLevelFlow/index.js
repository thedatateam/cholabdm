import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CholaLogo from '../../assets/images/cholaLogo.png';
import AssetTypeView from '../../components/AssetTypeView';
import AreaLevelSummary from '../../components/AreaLevelSummary';
import RegionApproveReworkButtons from "../../components/RegionApproveReworkButtons";
import { getRegionLevelSummary } from './../../actions/regionLevelFlow';
import RegionManagerFlow from '../../components/RegionManagerFlow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import RegionGraphVisual from '../../components/RegionGraphVisual';
import SubmitForApproval from "../../components/SubmitForApproval";


const RegionLevelFlow = ({ regionManager, getRegionLevelSummary }) => {
  document.title = 'RegionLevelFlow | CholaBDM';
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  const [error, setError] = React.useState({
    email: {
      msg: '',
      status: false,
    },
    password: {
      msg: '',
      status: false,
    },
  });
  const history = useHistory();
  const [regionManagerData, setRegionManagerData] = React.useState([]);
  const d = new Date();
  const n = monthNames[d.getMonth()];
  const area = localStorage.getItem('typeValue');
  const year = new Date().getFullYear().toString().substr(-2);
  const nextYear = parseInt(year) + 1;
  let role = localStorage.getItem('role');
  let type = localStorage.getItem('type');

  React.useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      getRegionLevelSummary(area, n, role);
    }
    return () => {
      isMounted = false;
    };
  }, []);
  if (regionManager && regionManager.getRegionLevelSummarySuccess === true) {
    if (regionManager.response && regionManager.response.overalldata)
      setRegionManagerData(regionManager.response.overalldata[0]);
    regionManager.getRegionLevelSummarySuccess = false;
  }
  const handleProductLevelInput = () => {
    // (productLevelOutput(area, n, role));

    window.location = '/RegionProductLevelInput';
  };
  function handleBack() {
    history.push('/login');
  }
  const goToFinalisedRateView = () => {
    history.push("/RegionFinalizedRate");
  }
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs>
          <img
            src={CholaLogo}
            style={{ margin: '10px', width: '150px', height: '100px' }}
          />
        </Grid>
        <Grid item xs style={{ textAlign: 'center', marginTop: '30px' }}>
          <Typography variant='h5' style={{ textAlign: 'center', fontWeight: '400' }}>Region Level Summary</Typography>
          {area ? <Typography variant='h6' style={{ textAlign: 'center', fontWeight: '200' }}> {area}</Typography> : null}
        </Grid>
        <Grid
          item
          xs
          style={{
            textAlign: 'right',
            marginTop: '50px',
            marginRight: '10px',
            fontWeight: 'bold',
          }}
        >
          Current Month : {n} FY {year}-{nextYear}
        </Grid>
      </Grid>
      <Button variant="contained"
         onClick={ handleBack}
          style={{ textTransform: 'none',backgroundColor: '#0275d8', margin: '10px', color: 'white' }}>
          Back </Button>
      <Paper elevation={3} style={{ margin: 20, padding: 20 }}>
        <RegionManagerFlow regionData={regionManagerData} />
        <Grid container justifyContent="flex-end">
          <Grid item>
        <Button
          variant="contained"
          onClick={handleProductLevelInput}
          style={{ textTransform: 'none',backgroundColor: '#0275d8', margin: '10px', color: 'white' }}
        >
          Product Level Input
        </Button>
        </Grid>
          <Grid item>
        <Button
          variant="contained"
          onClick={goToFinalisedRateView}
          style={{textTransform: 'none', backgroundColor: '#0275d8', margin: '10px', color: 'white' }}
        >
          Finalized Rate
        </Button>
        </Grid>
        </Grid>
      </Paper>
      <Paper elevation={3} style={{ margin: 20, padding: 20 }}>
        <AssetTypeView />
        {/* <SubmitForApproval /> */}
        <AreaLevelSummary />
        <RegionApproveReworkButtons />
      </Paper>
      <RegionGraphVisual />

    </div>
  );
};

const mapStateToProps = (state) => ({
  regionManager: state.regionManager,
});

const mapDispatchToProps = { getRegionLevelSummary };

export default connect(mapStateToProps, mapDispatchToProps)(RegionLevelFlow);
