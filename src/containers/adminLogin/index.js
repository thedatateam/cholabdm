import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import { Grid } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 520,
  },
}));

const AdminLogin = () => {
  const classes = useStyles();
  document.title = 'Admin Login | CholaBDM';
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const history = useHistory();

  function handleLogin(ev) {
    ev.preventDefault();
    if (name === 'admin' && password === 'admin123') {
      setName('');
      setPassword('');
      setError(null);
      history.push('/admin');
    } else {
      setError('Wrong username/password entered. Please try again!');
      setName('');
      setPassword('');
    }
  }

  return (
    <>
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={true}
        style={{ margin: 'auto' }}
      >
        <DialogTitle
          id="simple-dialog-title"
          style={{ marginTop: '30px', textAlign: 'center' }}
        >
          Admin Login
        </DialogTitle>

        <form style={{ padding: 30 }} onSubmit={handleLogin}>
          <Grid container direction="column" spacing={3}>
            <Grid item className={classes.formControl}>
              <TextField
                id="outlined-basic"
                label="User Name"
                variant="outlined"
                placeholder="user"
                fullWidth
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Grid>
            <Grid item className={classes.formControl}>
              <TextField
                type="password"
                id="outlined-basic2"
                label="Password"
                variant="outlined"
                placeholder="******"
                fullWidth
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Grid>
          </Grid>
          {error && (
            <Grid item>
              <p>{error}</p>
            </Grid>
          )}
          <Grid item style={{ display: 'flex', justifyContent: 'center' }}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              style={{
                textTransform: 'none',
                marginRight: '10px',
                alignSelf: 'center',
                width: '200px',
                margin: '20px',
              }}
            >
              Login
            </Button>
          </Grid>
        </form>
      </Dialog>
    </>
  );
};

export default AdminLogin;
