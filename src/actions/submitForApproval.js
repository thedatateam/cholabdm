import * as types from './actionTypes';
import areaManagerFlowAPI from '../api/areaManagerFlow';
import regionManagerFlowAPI from '../api/regionLevelFlow';
import { getCurrentMonth } from '../utils';

export function submitForApproval() {
  const area = localStorage.getItem('typeValue');
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return (dispatch) => {
    dispatch(submitForApprovalInitiated());
    areaManagerFlowAPI
      .submitForApproval(area, period, role)
      .then((result) => {
        dispatch(submitForApprovalSuccess(result));
      })
      .catch((err) => {
        dispatch(submitForApprovalFailure(err));
      });
  };
}

function submitForApprovalInitiated() {
  return { type: types.SUBMIT_FOR_APPROVAL_INITIATED };
}

function submitForApprovalFailure(error) {
  return { type: types.SUBMIT_FOR_APPROVAL_FAILURE, error };
}

function submitForApprovalSuccess(data) {
  return { type: types.SUBMIT_FOR_APPROVAL_SUCCESS, data };
}

export function submitForApprovalRM() {
  const region = localStorage.getItem('typeValue');
  // const region = 'TS';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return (dispatch) => {
    dispatch(submitForApprovalInitiatedRM());
    regionManagerFlowAPI
      .submitRegionResponse(region, period, role)
      .then((result) => {
        dispatch(submitForApprovalSuccessRM(result));
      })
      .catch((err) => {
        dispatch(submitForApprovalFailureRM(err));
      });
  };
}

function submitForApprovalInitiatedRM() {
  return { type: types.SUBMIT_FOR_APPROVAL_RM_INITIATED };
}

function submitForApprovalFailureRM(error) {
  return { type: types.SUBMIT_FOR_APPROVAL_RM_FAILURE, error };
}

function submitForApprovalSuccessRM(data) {
  return { type: types.SUBMIT_FOR_APPROVAL_RM_SUCCESS, data };
}

export function submitForReworkRM() {
  const region = localStorage.getItem('typeValue');
  // const region = 'TS';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return (dispatch) => {
    dispatch(submitForReworkInitiatedRM());
    regionManagerFlowAPI
      .reworkRegionResponse(region, period, role)
      .then((result) => {
        dispatch(submitForReworkSuccessRM(result));
      })
      .catch((err) => {
        dispatch(submitForReworkFailureRM(err));
      });
  };
}

function submitForReworkInitiatedRM() {
  return { type: types.SUBMIT_FOR_REWORK_RM_INITIATED };
}

function submitForReworkFailureRM(error) {
  return { type: types.SUBMIT_FOR_REWORK_RM_FAILURE, error };
}

function submitForReworkSuccessRM(data) {
  return { type: types.SUBMIT_FOR_REWORK_RM_SUCCESS, data };
}

export function submitForReworkCommentRM(body) {
  const region = localStorage.getItem('typeValue');
  // const region = 'TS';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return (dispatch) => {
    dispatch(submitForReworkCommentInitiatedRM());
    regionManagerFlowAPI
      .reworkRegionCommentResponse(region, period, role, body)
      .then((result) => {
        dispatch(submitForReworkCommentSuccessRM(result));
      })
      .catch((err) => {
        dispatch(submitForReworkCommentFailureRM(err));
      });
  };
}

function submitForReworkCommentInitiatedRM() {
  return { type: types.SUBMIT_FOR_REWORK_COMMENT_RM_INITIATED };
}

function submitForReworkCommentFailureRM(error) {
  return { type: types.SUBMIT_FOR_REWORK_COMMENT_RM_FAILURE, error };
}

function submitForReworkCommentSuccessRM(data) {
  return { type: types.SUBMIT_FOR_REWORK_COMMENT_RM_SUCCESS, data };
}
