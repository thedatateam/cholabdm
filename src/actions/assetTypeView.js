import * as types from './actionTypes';
import areaManagerFlowAPI from '../api/areaManagerFlow';
import regionLevelFlow from '../api/regionLevelFlow';
import zoneLevelFlow from '../api/zoneLevelFlow';
import { getCurrentMonth } from '../utils';

export function getAssetTypeView() {
  const area = localStorage.getItem('typeValue');
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getAssetTypeViewInitiated());
    try {
      const results = await Promise.all([
        areaManagerFlowAPI.getAllAssetView(area, period, role),
        areaManagerFlowAPI.getNewAssetView(area, period, role),
        areaManagerFlowAPI.getShubhAssetView(area, period, role),
        areaManagerFlowAPI.getUsedAssetView(area, period, role),
      ]);
      const [
        { overalldata: allData },
        { overalldata: newData },
        { overalldata: shubhData },
        { overalldata: usedData },
      ] = results;
      const payload = {
        allData,
        newData,
        shubhData,
        usedData,
      };
      dispatch(getAssetTypeViewSuccess(payload));
    } catch (err) {
      dispatch(getAssetTypeViewFailure(err.message));
    }
  };
}

function getAssetTypeViewInitiated() {
  return { type: types.GET_ASSET_VIEW_INITIATED };
}

function getAssetTypeViewFailure(error) {
  return { type: types.GET_ASSET_VIEW_FAILURE, error };
}

function getAssetTypeViewSuccess({ allData, usedData, shubhData, newData }) {
  return {
    type: types.GET_ASSET_VIEW_SUCCESS,
    data: { allData, usedData, shubhData, newData },
  };
}

export function getRMAssetTypeView() {
  const region = localStorage.getItem('typeValue');
  // const region = "TS";
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getRMAssetTypeViewInitiated());
    try {
      const results = await Promise.all([
        regionLevelFlow.getAllAssetView(region, period, role),
        regionLevelFlow.getNewAssetView(region, period, role),
        regionLevelFlow.getShubhAssetView(region, period, role),
        regionLevelFlow.getUsedAssetView(region, period, role),
      ]);
      const [
        { overalldata: allRMData },
        { overalldata: newRMData },
        { overalldata: shubhRMData },
        { overalldata: usedRMData },
      ] = results;
      const payload = {
        allRMData,
        newRMData,
        shubhRMData,
        usedRMData,
      };
      dispatch(getRMAssetTypeViewSuccess(payload));
    } catch (err) {
      dispatch(getRMAssetTypeViewFailure(err.message));
    }
  };
}

function getRMAssetTypeViewInitiated() {
  return { type: types.GET_RM_ASSET_VIEW_INITIATED };
}

function getRMAssetTypeViewFailure(error) {
  return { type: types.GET_RM_ASSET_VIEW_FAILURE, error };
}

function getRMAssetTypeViewSuccess({ allRMData, usedRMData, shubhRMData, newRMData }) {
  return {
    type: types.GET_RM_ASSET_VIEW_SUCCESS,
    data: { allRMData, usedRMData, shubhRMData, newRMData },
  };
}

export function getZMAssetTypeView() {
  const zone = localStorage.getItem('typeValue');
  // const zone = "South";
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getZMAssetTypeViewInitiated());
    try {
      const results = await Promise.all([
        zoneLevelFlow.getAllAssetView(zone, period, role),
        zoneLevelFlow.getNewAssetView(zone, period, role),
        zoneLevelFlow.getShubhAssetView(zone, period, role),
        zoneLevelFlow.getUsedAssetView(zone, period, role),
      ]);
      const [
        { overalldata: allZMData },
        { overalldata: newZMData },
        { overalldata: shubhZMData },
        { overalldata: usedZMData },
      ] = results;
      const payload = {
        allZMData,
        newZMData,
        shubhZMData,
        usedZMData,
      };
      dispatch(getZMAssetTypeViewSuccess(payload));
    } catch (err) {
      dispatch(getZMAssetTypeViewFailure(err.message));
    }
  };
}

function getZMAssetTypeViewInitiated() {
  return { type: types.GET_ZM_ASSET_VIEW_INITIATED };
}

function getZMAssetTypeViewFailure(error) {
  return { type: types.GET_ZM_ASSET_VIEW_FAILURE, error };
}

function getZMAssetTypeViewSuccess({ allZMData, usedZMData, shubhZMData, newZMData }) {
  return {
    type: types.GET_ZM_ASSET_VIEW_SUCCESS,
    data: { allZMData, usedZMData, shubhZMData, newZMData },
  };
}