import * as types from './actionTypes';
import areaManagerFlowAPI from '../api/areaManagerFlow';

export function getAreaLevelSummary(area,period,role) {
    return function (dispatch) {
        dispatch(getAreaLevelSummaryPending());
        return areaManagerFlowAPI.getAreaLevelSummary(area,period,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getAreaLevelSummarySuccess(userResponse));
            } else
                dispatch(getAreaLevelSummaryError(userResponse));
        }).catch(error => {
            dispatch(getAreaLevelSummaryError(error));
        });
    };
}
export function getFinalizedRate(area,offset) {
    return function (dispatch) {
        dispatch(getFinalizedRatePending());
        return areaManagerFlowAPI.getFinalizedRate(area,offset).then(userResponse => {
            if (userResponse && userResponse.FinalizedRate && userResponse.FinalizedRate.length) {
                dispatch(getFinalizedRateSuccess(userResponse));
            } else
                dispatch(getFinalizedRateError(userResponse));
        }).catch(error => {
            dispatch(getFinalizedRateError(error));
        });
    };
}

export function getFinalizedRatePending() {
    return {
        type: types.GET_FINALIIZED_RATE_INITIATED
    };
}
export function getFinalizedRateSuccess(finalizedRate) {
    return {
        type: types.GET_FINALIIZED_RATE_SUCCESS,
        finalizedRate
    };
}
export function getFinalizedRateError(finalizedRate) {
    return {
        type: types.GET_FINALIIZED_RATE_FAILURE,
        finalizedRate
    }
}
export function productLevelOutput(area,period,role) {
    return function (dispatch) {
        dispatch(productLevelInputPending());
        return areaManagerFlowAPI.productLevelOutput(area,period,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(productLevelInputSuccess(userResponse));
            } else
                dispatch(productLevelInputError(userResponse));
        }).catch(error => {
            dispatch(productLevelInputError(error));
        });
    };
}
export function getBDMTemplateStatus(area,period,role) {
    return function (dispatch) {
        dispatch(productLevelInputPending());
        return areaManagerFlowAPI.getBDMTemplateStatus(area,period,role).then(userResponse => {
            if (userResponse && userResponse.BDMTemplateStatus  && userResponse.BDMTemplateStatus.length>0 ) {
                dispatch(getBDMTemplateStatusSuccess(userResponse ));
            } else
                dispatch(getBDMTemplateStatusError(userResponse));
        }).catch(error => {
            dispatch(getBDMTemplateStatusError(error));
        });
    };
}
export function getBDMTemplateStatusPending() {
    return {
        type: types.GET_BDMT_TEMPLATE_STATUS_INITIATED
    };
}
export function getBDMTemplateStatusSuccess(templateStatus) {
    return {
        type: types.GET_BDMT_TEMPLATE_STATUS_SUCCESS,
        templateStatus
    };
}
export function getBDMTemplateStatusError(templateStatus) {
    return {
        type: types.GET_BDMT_TEMPLATE_STATUS_FAILURE,
        templateStatus
    }
}
export function updateProductLevelOutput(updatedData) {
    return function (dispatch) {
        dispatch(updateProductLevelOutputPending());
        return areaManagerFlowAPI.updateProductLevelOutput(updatedData).then(userResponse => {
            if (userResponse && !userResponse.body && !userResponse.statusCode) {
                dispatch(updateProductLevelOutputSuccess(userResponse));
            } else
                dispatch(updateProductLevelOutputError(userResponse));
        }).catch(error => {
            dispatch(updateProductLevelOutputError(error));
        });
    };
}
export function updateProductLevelOutputPending() {
    return {
        type: types.PUT_PRODUCT_LEVEL_UPDATE_INITIATED
    };
}
export function updateProductLevelOutputSuccess(updateProductInput) {
    return {
        type: types.PUT_PRODUCT_LEVEL_UPDATE_SUCCESS,
        updateProductInput
    };
}
export function updateProductLevelOutputError(error) {
    return {
        type: types.PUT_PRODUCT_LEVEL_UPDATE_FAILURE,
        error
    }
}
export function productLevelInputPending() {
    return {
        type: types.GET_PRODUCT_LEVEL_INITIATED
    };
}
export function productLevelInputSuccess(productInput) {
    return {
        type: types.GET_PRODUCT_LEVEL_SUCCESS,
        productInput
    };
}
export function productLevelInputError(productInput) {
    return {
        type: types.GET_PRODUCT_LEVEL_FAILURE,
        productInput
    }
}
export function getAreaLevelSummaryPending() {
    return {
        type: types.GET_AREA_LEVEL_SUMMARY_INITIATED
    };
}
export function getAreaLevelSummarySuccess(areaManager) {
    return {
        type: types.GET_AREA_LEVEL_SUMMARY_SUCCESS,
        areaManager
    };
}
export function getAreaLevelSummaryError(areaManager) {
    return {
        type: types.GET_AREA_LEVEL_SUMMARY_FAILURE,
        areaManager
    }
}

export function getNewAssetView(area,period,role) {
    return function (dispatch) {
        dispatch(getNewAssetViewPending());
        return areaManagerFlowAPI.getNewAssetView(area,period,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getNewAssetViewSuccess(userResponse));
            } else
                dispatch(getNewAssetViewError(userResponse));
        }).catch(error => {
            dispatch(getNewAssetViewError(error));
        });
    };
}
export function getNewAssetViewPending() {
    return {
        type: types.GET_NEW_ASSET_VIEW_INITIATED
    };
}
export function getNewAssetViewSuccess(newAssetType) {
    return {
        type: types.GET_NEW_ASSET_VIEW_SUCCESS,
        newAssetType
    };
}
export function getNewAssetViewError(newAssetType) {
    return {
        type: types.GET_NEW_ASSET_VIEW_FAILURE,
        newAssetType
    }
}

export function getCustomerSegmentGraphData(type,area,segment,period) {
    return function (dispatch) {
        dispatch(getCustomerSegmentGraphDataPending());
        return areaManagerFlowAPI.getCustomerSegmentGraphData(type,area,segment,period).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getCustomerSegmentGraphDataSuccess(userResponse));
            } else
                dispatch(getCustomerSegmentGraphDataError(userResponse));
        }).catch(error => {
            dispatch(getCustomerSegmentGraphDataError(error));
        });
    };
}
export function getCustomerSegmentGraphDataPending() {
    return {
        type: types.GET_SEGMENT_GRAPH_INITIATED
    };
}
export function getCustomerSegmentGraphDataSuccess(segmentGraphData) {
    return {
        type: types.GET_SEGMENT_GRAPH_SUCCESS,
        segmentGraphData
    };
}
export function getCustomerSegmentGraphDataError(segmentGraphData) {
    return {
        type: types.GET_SEGMENT_GRAPH_FAILURE,
        segmentGraphData
    }
}
export function getCustomerCategoryGraphData(type,area,segment,period) {
    return function (dispatch) {
        dispatch(getCustomerCategoryGraphDataPending());
        return areaManagerFlowAPI.getCustomerCategoryGraphData(type,area,segment,period).then(userResponse => {
            
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getCustomerCategoryGraphDataSuccess(userResponse));
            } else
                dispatch(getCustomerCategoryGraphDataError(userResponse));
        }).catch(error => {
            dispatch(getCustomerCategoryGraphDataError(error));
        });
    };
}
export function getCustomerCategoryGraphDataPending() {

    return {
        type: types.GET_CATEGORY_GRAPH_INITIATED
    };
}
export function getCustomerCategoryGraphDataSuccess(categoryGraphData) {
    return {
        type: types.GET_CATEGORY_GRAPH_SUCCESS,
        categoryGraphData
    };
}
export function getCustomerCategoryGraphDataError(categoryGraphData) {

    return {
        type: types.GET_CATEGORY_GRAPH_FAILURE,
        categoryGraphData
    }
}
export function getAllDropDown(type) {
    return function (dispatch) {
        dispatch(getAllDropDownPending());
        return areaManagerFlowAPI.getAllDropDown(type).then(userResponse => {
            if (userResponse && userResponse.AreasInRegion && userResponse.AreasInRegion.length) {
                dispatch(getAllDropDownSuccess(userResponse));
            } else
                dispatch(getAllDropDownError(userResponse));
        }).catch(error => {
            dispatch(getAllDropDownError(error));
        });
    };
}
export function getAllDropDownPending() {
    return {
        type: types.GET_ALL_DROP_DOWN_INITIATED
    };
}
export function getAllDropDownSuccess(allDropdown) {
    return {
        type: types.GET_ALL_DROP_DOWN_SUCCESS,
        allDropdown
    };
}
export function getAllDropDownError(allDropdown) {
    return {
        type: types.GET_ALL_DROP_DOWN_FAILURE,
        allDropdown
    }
}
export function searchFinalizedRate(flow,area,key,value) {
    return function (dispatch) {
        dispatch(searchFinalizedRatePending());
        return areaManagerFlowAPI.searchFinalizedRate(flow,area,key,value).then(userResponse => {
            if (userResponse && userResponse.SearchFinalizedRate && userResponse.SearchFinalizedRate.length) {
                dispatch(searchFinalizedRateSuccess(userResponse));
                
            } else
                dispatch(searchFinalizedRateError(userResponse));
        }).catch(error => {
            dispatch(searchFinalizedRateError(error));
        });
    };
}
export function searchFinalizedRatePending() {
    return {
        type: types.SEARCH_FINALIZED_RATE_INITIATED
    };
}
export function searchFinalizedRateSuccess(searchfinalizedRate) {
    return {
        type: types.SEARCH_FINALIZED_RATE_SUCCESS,
        searchfinalizedRate
    };
}
export function searchFinalizedRateError(searchfinalizedRate) {
    return {
        type: types.SEARCH_FINALIZED_RATE_FAILURE,
        searchfinalizedRate
    }
}