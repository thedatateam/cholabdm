import * as types from './actionTypes';
import areaLevelSummaryAPI from '../api/areaLevelSummary';
import zoneLevelSummaryAPI from '../api/zoneLevelFlow';
import { getCurrentMonth } from '../utils';

export function getRMAreaLevelSummary() {
  const area = localStorage.getItem('typeValue');
  // const area = 'TS';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getRMAreaLevelSummaryInitiated());
    try {
      const { overalldata } = await areaLevelSummaryAPI.getRMAreaLevelSummary(
        area,
        period,
        role
      );
      dispatch(getRMAreaLevelSummarySuccess(overalldata));
    } catch (err) {
      dispatch(getRMAreaLevelSummaryFailure(err));
    }
  };
}

function getRMAreaLevelSummaryInitiated() {
  return { type: types.GET_RM_AREA_LEVEL_SUMMARY_INITIATED };
}

function getRMAreaLevelSummaryFailure(error) {
  return { type: types.GET_RM_AREA_LEVEL_SUMMARY_FAILURE, error };
}

function getRMAreaLevelSummarySuccess(data) {
  return {
    type: types.GET_RM_AREA_LEVEL_SUMMARY_SUCCESS,
    data,
  };
}

export function getZMAreaLevelSummary() {
  const zone = localStorage.getItem('typeValue');
  // const zone = 'South';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getZMAreaLevelSummaryInitiated());
    try {
      const { overalldata } = await zoneLevelSummaryAPI.zoneAreaSummary(
        zone,
        period,
        role
      );
      dispatch(getZMAreaLevelSummarySuccess(overalldata));
    } catch (err) {
      dispatch(getZMAreaLevelSummaryFailure(err));
    }
  };
}

function getZMAreaLevelSummaryInitiated() {
  return { type: types.GET_RM_AREA_LEVEL_SUMMARY_INITIATED };
}

function getZMAreaLevelSummaryFailure(error) {
  return { type: types.GET_RM_AREA_LEVEL_SUMMARY_FAILURE, error };
}

function getZMAreaLevelSummarySuccess(data) {
  return {
    type: types.GET_RM_AREA_LEVEL_SUMMARY_SUCCESS,
    data,
  };
}
