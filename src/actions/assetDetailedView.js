import * as types from './actionTypes';
import areaManagerFlowAPI from '../api/areaManagerFlow';
import regionLevelFlowAPI from '../api/regionLevelFlow';
import zoneLevelFlowAPI from '../api/zoneLevelFlow';
import { getCurrentMonth } from '../utils';

export function getAssetDetailedView(segment, offset = 0, limit = 1000) {
  const area = localStorage.getItem('typeValue');
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getAssetDetailedViewInitiated());
    try {
      const data = await areaManagerFlowAPI.detailedView(
        area,
        period,
        role,
        segment,
        offset,
        limit
      );
      dispatch(getAssetDetailedViewSuccess(data));
    } catch (err) {
      dispatch(getAssetDetailedViewFailure(err));
    }
  };
}

function getAssetDetailedViewInitiated() {
  return { type: types.GET_ASSET_DETAILED_VIEW_INITIATED };
}

function getAssetDetailedViewFailure(error) {
  return { type: types.GET_ASSET_DETAILED_VIEW_FAILURE, error };
}

function getAssetDetailedViewSuccess(data) {
  return {
    type: types.GET_ASSET_DETAILED_VIEW_SUCCESS,
    data,
  };
}

export function getRMAssetDetailedView(segment, offset = 0, limit = 1000) {
  const region = localStorage.getItem('typeValue');
  // const region = 'TS';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getRMAssetDetailedViewInitiated());
    try {
      const data = await regionLevelFlowAPI.regionDetailedView(
        region,
        period,
        role,
        segment,
        offset,
        limit
      );
      dispatch(getRMAssetDetailedViewSuccess(data));
    } catch (err) {
      dispatch(getRMAssetDetailedViewFailure(err));
    }
  };
}

function getRMAssetDetailedViewInitiated() {
  return { type: types.GET_RM_ASSET_DETAILED_VIEW_INITIATED };
}

function getRMAssetDetailedViewFailure(error) {
  return { type: types.GET_RM_ASSET_DETAILED_VIEW_FAILURE, error };
}

function getRMAssetDetailedViewSuccess(data) {
  return {
    type: types.GET_RM_ASSET_DETAILED_VIEW_SUCCESS,
    data,
  };
}

export function getZMAssetDetailedView(segment, offset = 0, limit = 1000) {
  const zone = localStorage.getItem('typeValue');
  // const zone = 'South';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return async (dispatch) => {
    dispatch(getZMAssetDetailedViewInitiated());
    try {
      const data = await zoneLevelFlowAPI.zoneDetailedView(
        zone,
        period,
        role,
        segment,
        offset,
        limit
      );
      dispatch(getZMAssetDetailedViewSuccess(data));
    } catch (err) {
      dispatch(getZMAssetDetailedViewFailure(err));
    }
  };
}

function getZMAssetDetailedViewInitiated() {
  return { type: types.GET_ZM_ASSET_DETAILED_VIEW_INITIATED };
}

function getZMAssetDetailedViewFailure(error) {
  return { type: types.GET_ZM_ASSET_DETAILED_VIEW_FAILURE, error };
}

function getZMAssetDetailedViewSuccess(data) {
  return {
    type: types.GET_ZM_ASSET_DETAILED_VIEW_SUCCESS,
    data,
  };
}
