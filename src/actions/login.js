import * as types from './actionTypes';
import authApi from '../api/login';

export function login(token) {
    return function (dispatch) {
        dispatch(userLoginPending());
        return authApi.login(token).then(userResponse => {
            if (userResponse.authenticated===true) {
                dispatch(userLoginSuccess(userResponse));
            } else
                dispatch(userLoginError(userResponse));
        }).catch(error => {
            dispatch(userLoginFailed(error));
        });
    };
}
export function forgotPasswordTokenCreation(userParams) {
    return function (dispatch) {
        dispatch(forgotPasswordPending());
        return authApi.forgotPasswordTokenCreation(userParams).then(userResponse => {

            if ( userResponse.status === "SUCCESS") {
                dispatch(forgotPasswordSuccess(userResponse));
            } else{
                dispatch(forgotPasswordError(userResponse));
            }
        }).catch(error => {
            dispatch(forgotPasswordError(error));
        });
    };
}

export function reset( area,period,role) {
    return function (dispatch) {
        dispatch(resetPasswordPending());
        return authApi.reset(area,period,role).then(userResponse => {
            if (userResponse) {
                dispatch(resetPasswordSuccess(userResponse));
            } else
                dispatch(resetPasswordError(userResponse));
        }).catch(error => {
            dispatch(resetPasswordError(error));
        });
    };
}
export function logout() {
    return function (dispatch) {
        dispatch(userLogoutPending());
        return authApi.logout().then(userResponse => {
            if (userResponse.status && userResponse.status === "success") {
                dispatch(userLogoutSuccess(userResponse));
            } else
                dispatch(userLogoutError(userResponse));
        }).catch(error => {
            dispatch(userLogoutError(error));
        });
    };
}
export function validate(userParams) {
    return function (dispatch) {
        dispatch(validateTokenPending());
        return authApi.validate(userParams).then(userResponse => {
            if (userResponse.statusDTO && userResponse.statusDTO.status === "SUCCESS") {
                dispatch(validateTokenSuccess(userResponse));
            } else{

                dispatch(validateTokenError(userResponse));
            }
        }).catch(error => {
            dispatch(validateTokenError(error));
        });
    };
}

export function userLoginPending() {
    return {
        type: types.LOGIN_INITIATED
    };
}
export function userLoginSuccess(user) {
    return {
        type: types.LOGIN_SUCCESS,
        user
    };
}
export function userLoginError(user) {
    return {
        type: types.LOGIN_INVALID_CREDS,
        user
    }
}
export function userLoginFailed(error) {
    return {
        type: types.LOGIN_FAILURE,
        error
    };
}
export function forgotPasswordPending() {
    return {
        type: types.REQUEST_PASSWORD_INITIATED
    };
}
export function forgotPasswordSuccess(user) {
    return {
        type: types.REQUEST_PASSWORD_SUCCESS,
        user
    };
}
export function forgotPasswordError(user) {
    return {
        type: types.REQUEST_PASSWORD_FAILURE,
        user
    }
}
export function resetPasswordPending() {
    return {
        type: types.RESET_PASSWORD_INITIATED
    };
}
export function resetPasswordSuccess(user) {
    return {
        type: types.RESET_PASSWORD_SUCCESS,
        user
    };
}
export function resetPasswordError(user) {
    return {
        type: types.RESET_PASSWORD_FAILURE,
        user
    }
}
export function changePasswordPending() {
    return {
        type: types.CHANGE_PASSWORD_INITIATED
    };
}
export function changePasswordSuccess(user) {
    return {
        type: types.CHANGE_PASSWORD_SUCCESS,
        user
    };
}
export function changePasswordError(user) {
    return {
        type: types.CHANGE_PASSWORD_FAILURE,
        user
    }
}
export function userLogoutPending() {
    return {
        type: types.LOGOUT_INITIATED
    };
}
export function userLogoutSuccess(user) {
    return {
        type: types.LOGOUT_SUCCESS,
        user
    };
}
export function userLogoutError(user) {
    return {
        type: types.LOGOUT_SUCCESS,
        user
    }
}
export function validateTokenPending() {
    return {
        type: types.VALIDATE_TOKEN_INITIATED
    };
}
export function validateTokenSuccess(user) {
    return {
        type: types.VALIDATE_TOKEN_SUCCESS,
        user
    };
}
export function validateTokenError(error) {
    return {
        type: types.VALIDATE_TOKEN_FAILURE,
        error
    }
}
