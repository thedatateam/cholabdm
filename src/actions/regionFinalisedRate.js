import regionLevelFlowAPI from '../api/regionLevelFlow';
import * as types from './actionTypes';

export function getRegionFinalizedRate(offset = 0, limit = 1000) {
  const name = localStorage.getItem('typeValue');
  return async (dispatch) => {
    dispatch(getFinalisedRateInitiated());
    try {
      const data = await regionLevelFlowAPI.getRegionFinalizedRate(
        name,
        offset,
        limit
      );
      dispatch(getFinalisedRateSuccess(data));
    } catch (err) {
      dispatch(getFinalisedRateFailure(err));
    }
  };
}

function getFinalisedRateInitiated() {
  return { type: types.GET_RM_FINALIIZED_RATE_INITIATED };
}

function getFinalisedRateFailure(error) {
  return { type: types.GET_RM_FINALIIZED_RATE_FAILURE, error };
}

function getFinalisedRateSuccess(data) {
  return {
    type: types.GET_RM_FINALIIZED_RATE_SUCCESS,
    data,
  };
}
