import * as types from './actionTypes';
import regionManagerFlowAPI from '../api/regionLevelFlow';
import zoneManagerFlowAPI from '../api/zoneLevelFlow';
import { getCurrentMonth } from '../utils';

export function getRegionLevelSummary(region,period,role) {
    return function (dispatch) {
        dispatch(getRegionLevelSummaryPending());
        return regionManagerFlowAPI.getRegionLevelSummary(region,period,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getRegionLevelSummarySuccess(userResponse));
            } else
                dispatch(getRegionLevelSummaryError(userResponse));
        }).catch(error => {
            dispatch(getRegionLevelSummaryError(error));
        });
    };
}

export function getRegionLevelSummaryPending() {
    return {
        type: types.GET_REGION_LEVEL_SUMMARY_INITIATED
    };
}
export function getRegionLevelSummarySuccess(regionManager) {
    return {
        type: types.GET_REGION_LEVEL_SUMMARY_SUCCESS,
        regionManager
    };
}
export function getRegionLevelSummaryError(regionManager) {
    return {
        type: types.GET_REGION_LEVEL_SUMMARY_FAILURE,
        regionManager
    }
}

//
export function regionProductOutput(area,period,role) {
    return function (dispatch) {
        dispatch(regionProductInputPending());
        return  regionManagerFlowAPI.regionProductOutput(area,period,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(regionProductInputSuccess(userResponse));
            } else
                dispatch(regionProductInputError(userResponse));
        }).catch(error => {
            dispatch(regionProductInputError(error));
        });
    };
}
export function regionProductInputPending() {
    return {
        type: types.GET_REGION_PRODUCT_LEVEL_INITIATED
    };
}
export function regionProductInputSuccess(regionProductInput) {
    return {
        type: types.GET_REGION_PRODUCT_LEVEL_SUCCESS,
        regionProductInput
    };
}
export function regionProductInputError(regionProductInput) {
    return {
        type: types.GET_REGION_PRODUCT_LEVEL_FAILURE,
        regionProductInput
    }
}
export function updateRegionProductLevelOutput(updatedData) {
    return function (dispatch) {
        dispatch(updateRegionProductLevelOutputPending());
        return  regionManagerFlowAPI.updateRegionProductLevelOutput(updatedData).then(userResponse => {
            if (userResponse && !userResponse.body && !userResponse.statusCode) {
                dispatch(updateRegionProductLevelOutputSuccess(userResponse));
            } else
                dispatch(updateRegionProductLevelOutputError(userResponse));
        }).catch(error => {
            dispatch(updateRegionProductLevelOutputError(error));
        });
    };
}
export function updateRegionProductLevelOutputPending() {
    return {
        type: types.PUT_REGION_PRODUCT_LEVEL_UPDATE_INITIATED
    };
}
export function updateRegionProductLevelOutputSuccess(updateRegionProductInput) {
    return {
        type: types.PUT_REGION_PRODUCT_LEVEL_UPDATE_SUCCESS,
        updateRegionProductInput
    };
}
export function updateRegionProductLevelOutputError(error) {
    return {
        type: types.PUT_REGION_PRODUCT_LEVEL_UPDATE_FAILURE,
        error
    }
}
export function getGraphData(region,role) {
    return function (dispatch) {
        dispatch(getGraphDataPending());
        return regionManagerFlowAPI.getGraphData(region,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getGraphDataSuccess(userResponse));
            } else
                dispatch(getGraphDataError(userResponse));
        }).catch(error => {
            dispatch(getGraphDataError(error));
        });
    };
}

export function getGraphDataPending() {
    return {
        type: types.GET_GRAPH_DATA_INITIATED
    };
}
export function getGraphDataSuccess(graphData) {
    return {
        type: types.GET_GRAPH_DATA_SUCCESS,
        graphData
    };
}
export function getGraphDataError(graphData) {
    return {
        type: types.GET_GRAPH_DATA_FAILURE,
        graphData
    }
}

export function getZMRegionLevelSummary() {
    const zone = localStorage.getItem('typeValue');
    // const zone = 'South';
    const period = getCurrentMonth();
    const role = localStorage.getItem('role');
    return async (dispatch) => {
        getZMRegionLevelSummaryInitiated()
        try {
            const {overalldata} = await zoneManagerFlowAPI.zoneRegionSummary(zone, period, role);
            dispatch(getZMRegionLevelSummarySuccess(overalldata));
        } catch (err) {
            dispatch(getZMRegionLevelSummaryFailure(err));
        }
    }
}

function getZMRegionLevelSummaryInitiated() {
    return { type: types.GET_ZM_REGION_LEVEL_SUMMARY_INITIATED };
  }
  
  function getZMRegionLevelSummaryFailure(error) {
    return { type: types.GET_ZM_REGION_LEVEL_SUMMARY_FAILURE, error };
  }
  
  function getZMRegionLevelSummarySuccess(data) {
    return {
      type: types.GET_ZM_REGION_LEVEL_SUMMARY_SUCCESS,
      data,
    };
  }
