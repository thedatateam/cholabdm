import * as types from './actionTypes';
import zoneManagerFlowAPI from '../api/zoneLevelFlow';

export function getZoneLevelSummary(zone,period,role) {
    return function (dispatch) {
        dispatch(getZoneLevelSummaryPending());
        return zoneManagerFlowAPI.getZoneLevelSummary(zone,period,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getZoneLevelSummarySuccess(userResponse));
            } else
                dispatch(getZoneLevelSummaryError(userResponse));
        }).catch(error => {
            dispatch(getZoneLevelSummaryError(error));
        });
    };
}

export function getZoneLevelSummaryPending() {
    return {
        type: types.GET_ZONE_LEVEL_SUMMARY_INITIATED
    };
}
export function getZoneLevelSummarySuccess(zoneManager) {
    return {
        type: types.GET_ZONE_LEVEL_SUMMARY_SUCCESS,
        zoneManager
    };
}
export function getZoneLevelSummaryError(zoneManager) {
    return {
        type: types.GET_ZONE_LEVEL_SUMMARY_FAILURE,
        zoneManager
    }
}

//
export function getZoneFinalizedRate(zone,offset) {
    return function (dispatch) {
        dispatch(getZoneFinalizedRatePending());
        return zoneManagerFlowAPI.getZoneFinalizedRate(zone,offset).then(userResponse => {
            if (userResponse && userResponse.FinalizedRate && userResponse.FinalizedRate.length) {
                dispatch(getZoneFinalizedRateSuccess(userResponse));
            } else
                dispatch(getZoneFinalizedRateError(userResponse));
        }).catch(error => {
            dispatch(getZoneFinalizedRateError(error));
        });
    };
}

export function getZoneFinalizedRatePending() {
    return {
        type: types.GET_ZONE_FINALIZED_RATE_INITIATED
    };
}
export function getZoneFinalizedRateSuccess(zoneManager) {
    return {
        type: types.GET_ZONE_FINALIZED_RATE_SUCCESS,
        zoneManager
    };
}
export function getZoneFinalizedRateError(zoneManager) {
    return {
        type: types.GET_ZONE_FINALIZED_RATE_FAILURE,
        zoneManager
    }
}

//
export function zoneProductOutput(area,period,role) {
    return function (dispatch) {
        dispatch(zoneProductInputPending());
        return  zoneManagerFlowAPI.zoneProductOutput(area,period,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(zoneProductInputSuccess(userResponse));
            } else
                dispatch(zoneProductInputError(userResponse));
        }).catch(error => {
            dispatch(zoneProductInputError(error));
        });
    };
}
export function zoneProductInputPending() {
    return {
        type: types.GET_ZONE_PRODUCT_LEVEL_INITIATED
    };
}
export function zoneProductInputSuccess(zoneProductInput) {
    return {
        type: types.GET_ZONE_PRODUCT_LEVEL_SUCCESS,
        zoneProductInput
    };
}
export function zoneProductInputError(zoneProductInput) {
    return {
        type: types.GET_ZONE_PRODUCT_LEVEL_FAILURE,
        zoneProductInput
    }
}
export function updateZoneProductLevelOutput(updatedData) {
    return function (dispatch) {
        dispatch(updateZoneProductLevelOutputPending());
        return  zoneManagerFlowAPI.updateZoneProductLevelOutput(updatedData).then(userResponse => {
            if (userResponse && !userResponse.body && !userResponse.statusCode) {
                dispatch(updateZoneProductLevelOutputSuccess(userResponse));
            } else
                dispatch(updateZoneProductLevelOutputError(userResponse));
        }).catch(error => {
            dispatch(updateZoneProductLevelOutputError(error));
        });
    };
}
export function updateZoneProductLevelOutputPending() {
    return {
        type: types.PUT_ZONE_PRODUCT_LEVEL_UPDATE_INITIATED
    };
}
export function updateZoneProductLevelOutputSuccess(updateZoneProductInput) {
    return {
        type: types.PUT_ZONE_PRODUCT_LEVEL_UPDATE_SUCCESS,
        updateZoneProductInput
    };
}
export function updateZoneProductLevelOutputError(error) {
    return {
        type: types.PUT_ZONE_PRODUCT_LEVEL_UPDATE_FAILURE,
        error
    }
}
export function getGraphData(zone,role) {
    return function (dispatch) {
        dispatch(getGraphDataPending());
        return zoneManagerFlowAPI.getGraphData(zone,role).then(userResponse => {
            if (userResponse && userResponse.overalldata && userResponse.overalldata.length) {
                dispatch(getGraphDataSuccess(userResponse));
            } else
                dispatch(getGraphDataError(userResponse));
        }).catch(error => {
            dispatch(getGraphDataError(error));
        });
    };
}

export function getGraphDataPending() {
    return {
        type: types.GET_GRAPH_DATA_INITIATED
    };
}
export function getGraphDataSuccess(graphData) {
    return {
        type: types.GET_GRAPH_DATA_SUCCESS,
        graphData
    };
}
export function getGraphDataError(graphData) {
    return {
        type: types.GET_GRAPH_DATA_FAILURE,
        graphData
    }
}
