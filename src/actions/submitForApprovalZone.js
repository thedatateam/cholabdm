import * as types from './actionTypes';
import zoneManagerFlowAPI from '../api/zoneLevelFlow';
import { getCurrentMonth } from '../utils';

export function submitForApproval() {
  const zone = localStorage.getItem('typeValue');
  // const zone = 'South';
  const period = getCurrentMonth();
  const role = localStorage.getItem('role');
  return (dispatch) => {
    dispatch(submitForApprovalInitiated());
    zoneManagerFlowAPI
      .submitForApproval(zone, period, role)
      .then((result) => {
        dispatch(submitForApprovalSuccess(result));
      })
      .catch((err) => {
        dispatch(submitForApprovalFailure(err));
      });
  };
}

function submitForApprovalInitiated() {
  return { type: types.SUBMIT_FOR_APPROVAL_ZM_INITIATED };
}

function submitForApprovalFailure(error) {
  return { type: types.SUBMIT_FOR_APPROVAL_ZM_FAILURE, error };
}

function submitForApprovalSuccess(data) {
  return { type: types.SUBMIT_FOR_APPROVAL_ZM_SUCCESS, data };
}
