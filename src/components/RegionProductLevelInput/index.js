import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//material-ui components
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Slide from '@material-ui/core/Slide';
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import CholaLogo from '../../assets/images/cholaLogo.png';
import * as regionLevelActions from '../../actions/regionLevelFlow';
import * as areaManagerActions from "../../actions/areaManagerFlow";

//other libraries
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import classNames from 'classnames';
import { SnackbarProvider, withSnackbar } from 'notistack';
import './RegionProductLevelInput.scss'
const moment = require("moment");
let editStatus=false;

function Transition(props) {
  return <Slide direction="up" {...props} />;
}
const styles = theme => ({
  
  root: {
    height: '100%',
  },
  agheader:{
    backgroundColors:"green"
  }
  
});

class RegionProductLevelInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
        regionProductInput:[],
        error:'',
        area:'',
        month:'',
        undoRedoCellEditingLimit: 50,
        role:'',
      columnDefs: [
        {headerName: " SEGMENT", field: "Segment",width: 120, suppressMenu: true, tooltip: (params) => params.value, editable: false,    },
        {headerName: "NEW OR USED", field: "NewOrUsed", width: 120,sortingOrder: ['asc'], suppressMenu: true, tooltip: (params) => params.value,editable: false,},
        {headerName: "ASSETDESC", field: "Assetdesc",width:  120,  suppressMenu: true, tooltip: (params) => params.value,editable: false,},
        {headerName: "ACTUAL ROTA", field: "Actual_ROTA",width: 160,  suppressMenu: true, tooltip: (params) => params.value,valueGetter: actualRotaGetter,editable: false,},
        {headerName: "PLANNED ROTA",field: "Book_PBT_ROTA",width:  160, valueGetter: plannedRotaGetter,editable: false, },
        {headerName: "ACTUAL IRR(LAST MONTH)",field: "Actual_IRR",width:  200,valueGetter: actualIrrGetter,editable: false, },
        {headerName: "PLANNED IRR(LAST MONTH)",field: "Prev_Planned_IRR" ,width:200,valueGetter: planneprvMonthIrrGetter ,editable: false,},
        {headerName: "PLANNED IRR(%)",field: "Planned_IRR",width:  160, valueGetter: plannedIrrGetter,headerClass: 'header-green',  cellStyle: cellStyle ,editable:!editStatus, },
    ],
    undoError:false,
    editStack : JSON.parse(localStorage.getItem('ProdLevelEditStack')) || [],
    resetError:false,
    resetStack : JSON.parse(localStorage.getItem('ProdLevelEditStack')) || [],
      gridOptions: {
        defaultColDef: {
          resizable: true,
          sortable: true,
          editable:true
        },
        currentInstance: this,
        localeText: { noRowsToShow: 'No Data Found' },

      },
 }
 this.undo = this.undo.bind(this);
 this.onGridReady = this.onGridReady.bind(this);
 this.setValue = this.setValue.bind(this);
 this.disable = this.disable.bind(this);
 this.reset = this.reset.bind(this);

 



  };
  
 
  componentDidMount() {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];

    const d = new Date();
    var n = monthNames[d.getMonth()]
    this.setState({month:n});
    var year = new Date().getFullYear().toString().substr(-2);
    let nextYear = parseInt(year) + 1;
    let role = localStorage.getItem('role');
    this.setState({role:role})
    let area = localStorage.getItem('typeValue');
    this.setState({area:area});
    let type = localStorage.getItem('type');
  this.props.actions.regionProductOutput(area, n, role);
  this.props.actions.getBDMTemplateStatus(area,n);



  }
  handleChange(prop,event) {
    this.setState({ [prop]: event.target.value });
  };
 
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }
  onFirstDataRendered (){
  
    this.disable('#undoBtn', true);
  
  };

  onCellValueChanged (params) {
      if(params && params.data)
      var paramData=params.data;
    var undoSize = params.api.getCurrentUndoSize();
    let productData={
        Region: this.state.area,
        Assetdesc:paramData.Assetdesc?paramData.Assetdesc:'0',
        NewOrUsed: paramData.NewOrUsed,
        Period:this.state.month,
        Planned_IRR: parseFloat(params.newValue).toFixed(2),
        Segment:paramData.Segment,
        Planned_IRR_old:parseFloat(params.oldValue).toFixed(2)
    };
    this.props.actions.updateRegionProductLevelOutput([productData]);

    this.disable('#undoBtn', undoSize < 1);
    var redoSize = params.api.getCurrentRedoSize();
   
    const stack = this.state.editStack ? this.state.editStack : [];
    this.setState({editStack: [...stack, { Stackid: params.rowIndex }]}, () => {
      localStorage.setItem('ProdLevelEditStack', JSON.stringify(this.state.editStack));
    });
  };

   disable(id, disabled) {
    document.querySelector(id).disabled = disabled;
  }
   setValue(id, value) {
    document.querySelector(id).value = value;
  }
  static getDerivedStateFromProps(nextProps,state,props) {
    if (nextProps.areaManager.getBDMTemplateStatusSuccess===true && nextProps.areaManager.responseBDM) {
      nextProps.areaManager.getBDMTemplateStatusSuccess = false;
      if(nextProps.areaManager.responseBDM.BDMTemplateStatus[0].ReadOnly===true)
      {nextProps.enqueueSnackbar("Edit is Disabled", { 
        variant: 'error',
    })}      
    return {
         getBDMTemplateStatus : nextProps.areaManager.responseBDM.BDMTemplateStatus[0],
         editStatus:nextProps.areaManager.responseBDM.BDMTemplateStatus[0].ReadOnly
      }
  }
      if (nextProps.regionManager.regionProductInputSuccess===true && nextProps.regionManager.response) {
        nextProps.regionManager.regionProductInputSuccess = false;
        return {
           regionProductInput : nextProps.regionManager.response.overalldata
        }
    }
    if (nextProps.regionManager.regionProductInputError===true) {
        nextProps.regionManager.regionProductInputError = false;
        return {
            regionProductInput: [],
        }
    }
    if (nextProps.regionManager. updateRegionProductLevelOutputSuccess===true && nextProps.regionManager.response) {
        nextProps.regionManager. updateRegionProductLevelOutputSuccess = false;
        // nextProps.actions.regionProductInput(currentState.area, currentState.month, currentState.role);

        return {
            undoError:false,

        }
    }
    if (nextProps.regionManager. updateRegionProductLevelOutputError===true && nextProps.regionManager.error) {
        nextProps.regionManager. updateRegionProductLevelOutputError = false;
        {nextProps.enqueueSnackbar(nextProps.regionManager.error.body, { 
            variant: 'error',
        })}
        return {
            undoError:true,
            error:nextProps.regionManager.error.body
        }
    }
  return null;
}

undo(params)  {
  const newStack = JSON.parse(JSON.stringify(this.state.editStack));

  newStack.pop();
  this.setState({undoError:false, editStack: newStack}, () => {
    localStorage.setItem('ProdLevelEditStack', JSON.stringify(this.state.editStack));
  })
  this.gridApi.undoCellEditing();
};
  reset(params)  {
    const newStack = JSON.parse(JSON.stringify(this.state.editStack));
  
    newStack.pop();
    this.setState({resetError:false, resetStack: newStack}, () => {
      localStorage.setItem('ProdLevelEditStack', JSON.stringify(this.state.editStack));
    })
    newStack.map(( idx) => (
      this.gridApi.undoCellEditing()
  
  ));
  };
  handleSummaryPage (){
    window.location = '/regionLevelFlow';  
  }
   
  render() {
    const { classes  } = this.props;
    if(this.state.undoError && this.state.undoError===true){
        this.undo();
    }
    return(
 <div>
      <Grid container>
        <Grid item xs={3}>
          <img src={CholaLogo} style={{ margin: '10px', width: '150px', height: '100px' }} />
        </Grid>
        <Grid item xs={6} style={{ textAlign: 'center', marginTop: '30px' }}>
          <Typography variant='h6' style={{ fontWeight: '200' }}>Product Level Input</Typography>
          {this.state.area ? <Typography variant='h6' style={{ textAlign: 'center', fontWeight: '100' }}> {this.state.area}</Typography> : null}
        </Grid>
       
      </Grid>
      <Button variant="contained"
          onClick={this.handleSummaryPage}
          style={{ textTransform: 'none',backgroundColor: '#0275d8', margin: '10px', color: 'white'}}>
            Back To Summary      </Button>
      <Paper elevation={3} style={{ margin: '10px',marginTop:'0px' ,padding: '10px' }}>

     <Grid container spacing={2}>
     <div class='box green'></div> Editable Fields
     <div class='box blue'></div>Less than Previous Actual_IRR
     <div class='box purple'></div>Less than Previous Planned_IRR
     <div class='box orange'></div>Less than Actual ROTA
        <Grid item xs style={{ textAlign: 'right', marginTop: '50px', marginRight: '10px', fontWeight: 'bold' }}>
        <Button variant="contained"
                        id="undoBtn"
                        onClick={() => this.undo()}
                        style={{ backgroundColor: '#0275d8', margin: '10px', color: 'white'  }}>
        Undo      </Button>        
        <Button variant="contained"
                        onClick={() => this.reset()}
                        style={{ backgroundColor: '#0275d8', margin: '10px', color: 'white'}}>
            Reset      </Button></Grid>
      </Grid>
   <div 
                      className="ag-theme-alpine"
                      style={{ 
                      height: '450px', 
                      width: '100%', }} 
                    >
                        <AgGridReact
                           rowData={this.state.regionProductInput}
                            columnDefs={this.state.columnDefs}
                            defaultColDef={this.state.defaultColDef}
                            onGridReady={this.onGridReady}
                            pagination={true}
                            gridOptions={this.state.gridOptions}
                            paginationPageSize={this.state.paginationPageSize}
                            enableRangeSelection={true}
                            enableFillHandle={true}
                            undoRedoCellEditing={true}
                            undoRedoCellEditingLimit={this.state.undoRedoCellEditingLimit}
                            enableCellChangeFlash={true}
                            onGridReady={this.onGridReady}
                            onFirstDataRendered={this.onFirstDataRendered.bind(this)}
                            onCellValueChanged={this.onCellValueChanged.bind(this)}
                            >
                        </AgGridReact>
                    </div>
                </Paper>
      </div>
    );
  }
}
function cellStyle(params) {
    if(parseFloat(params.value) < (parseFloat(params.data.Actual_IRR).toFixed(2)))
  return { backgroundColor: '#ABD6DF' };
  else if(parseFloat(params.value) < (parseFloat(params.data.Prev_Planned_IRR.toFixed(2))))
  return { backgroundColor: '#7F6084' };
  else if(parseFloat(params.value) < (parseFloat(params.data.Actual_ROTA.toFixed(2))))
  return { backgroundColor: '#FF8533' };
  else
  return{}

}

function actualRotaGetter(params) {
    if(params.data.Actual_ROTA){
          return  ( (typeof params.data.Actual_ROTA==='number')?(params.data.Actual_ROTA).toFixed(2):(parseFloat(params.data.Actual_IRR).toFixed(2)) )
        }
     return '0'
   }
   function plannedRotaGetter(params) {
    if(params.data.Book_PBT_ROTA){
        return  ( (typeof params.data.Book_PBT_ROTA==='number')?(params.data.Book_PBT_ROTA).toFixed(2):(parseFloat(params.data.Book_PBT_ROTA).toFixed(2)) )
    }
     return '0'
   }
   function actualIrrGetter(params) {
    if(params.data.Actual_IRR){
        return  ( (typeof params.data.Actual_IRR==='number')?(params.data.Actual_IRR).toFixed(2):(parseFloat(params.data.Actual_IRR).toFixed(2)) )
    }
     return '0'
   }
    function planneprvMonthIrrGetter(params) {
    if(params.data.Prev_Planned_IRR){
        return  ( (typeof params.data.Prev_Planned_IRR==='number')?(params.data.Prev_Planned_IRR).toFixed(2):(parseFloat(params.data.Prev_Planned_IRR).toFixed(2)) )
    }
     return '0'
   }
   function plannedIrrGetter(params) {
    if(params.data.Planned_IRR){
        return  ( (typeof params.data.Planned_IRR==='number')?(params.data.Planned_IRR).toFixed(2):(parseFloat(params.data.Planned_IRR).toFixed(2)) )
    }
     return '0'
   }
  
const mapStateToProps = state => ({
    regionManager: state.regionManager,
    areaManager: state.areaManager

  });
  
const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...regionLevelActions,...areaManagerActions,}, dispatch)

  };
};

const WrappedRegionProductLevelInput =  withMobileDialog()(withStyles(styles)(withSnackbar(RegionProductLevelInput)));

class RegionProductLevelInputWithSnackBar extends React.Component {
  render() {
    return (
      <SnackbarProvider 
      maxSnack={10}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}>
        <WrappedRegionProductLevelInput {...this.props} />
      </SnackbarProvider>
    );
  }
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegionProductLevelInputWithSnackBar);
