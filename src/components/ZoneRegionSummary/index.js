import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { getZMRegionLevelSummary } from '../../actions/regionLevelFlow';
import { formatNumbers } from '../../utils';

const useStyles = makeStyles({
  formControl: {
    margin: '10px',
    minWidth: 200,
  },
});

const ZoneRegionSummary = () => {
  let gridApi;
  let gridColumnApi;
  const classes = useStyles();
  const dispatch = useDispatch();
  const regionManager = useSelector((state) => state.zoneRegionSummary);
  const gridOptions = {
    defaultColDef: {
      resizable: true,
      sortable: true,
    },
    suppressHorizontalScroll: false,
    localeText: { noRowsToShow: 'No Data Found' },
  };

  const columnDefs = [
    {
      headerName: 'Region',
      field: 'Region',
      suppressMenu: true,
    },
    {
      headerName: 'Segment',
      field: 'Segment',
      suppressMenu: true,
    },
    {
      headerName: 'Actual Rota(%)',
      field: 'Actual_ROTA',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned Rota(%)',
      field: 'Planned_ROTA',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference Rota(%)',
      field: 'ROTADiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual NCL(%)',
      field: 'Actual_NCL',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned NCL(%)',
      field: 'Planned_NCL',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference NCL(%)',
      field: 'NCLDiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual IRR(%)',
      field: 'Actual_IRR',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned IRR(%)',
      field: 'Planned_IRR',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference IRR(%)',
      field: 'IRRDiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual Value(in Lakhs)',
      field: 'Actual_Value',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned Value(in Lakhs)',
      field: 'Planned_Value',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Bud Clossing Asset(in Lakhs)',
      field: 'Closing_Assets',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual Clossing Asset(in Lakhs)',
      field: 'Bud_Closing_Assets',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
  ];

  React.useEffect(() => {
    dispatch(getZMRegionLevelSummary());
  }, []);

  React.useEffect(() => {
  }, [regionManager]);

  const onGridReady = (params) => {
    // console.log(params);
    gridApi = params.api;
    gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
  };

  return (
    <>
      <Grid container spacing={3} direction="column">
        <Grid item lg={12} style={{ textAlign: 'center' }}>
          Region Level Summary
        </Grid>
        <Grid item lg={12}>
          <FormControl className={classes.formControl}>
            <InputLabel id="assetType">Segment</InputLabel>
            <Select
              labelId="assetType"
              id="type"
              name="type"
              defaultValue="default"
            >
              <MenuItem value={'default'}>default</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item lg={12}>
          {regionManager && regionManager.statusZM === 'SUCCESS' && (
            <div
              className="ag-theme-alpine"
              style={{
                height: '520px',
                width: '100%',
                overflowX: 'hidden',
              }}
            >
              <AgGridReact
                rowData={regionManager.regionDataZM}
                columnDefs={columnDefs}
                pagination={false}
                onGridReady={onGridReady}
                gridOptions={gridOptions}
              />
            </div>
          )}
        </Grid>
      </Grid>
    </>
  );
};

export default ZoneRegionSummary;
