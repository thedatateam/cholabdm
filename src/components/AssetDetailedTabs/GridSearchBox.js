import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  input: {
    margin: theme.spacing(1),
    width: '250px',
  },
}));

const GridSearchBox = ({ onValueChange }) => {
  const classes = useStyles();

  function handleChange(event) {
    const { value } = event.target;
    onValueChange(value);
  }

  return (
    <>
      <TextField
        className={classes.input}
        id="grid-search"
        name="grid-search"
        label="Search"
        variant="outlined"
        onChange={handleChange}
      />
    </>
  );
};

export default GridSearchBox;
