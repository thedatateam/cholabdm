import React from 'react';
import Grid from '@material-ui/core/Grid';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import PageSizeSelect from './PageSizeSelect';
import GridSearchBox from './GridSearchBox';

const Summary = ({ details }) => {
  let gridApi;
  let gridColumnApi;

  const gridOptions = {
    defaultColDef: {
      resizable: true,
      sortable: true,
    },
    suppressHorizontalScroll: false,
    paginationPageSize: 10,
    cacheBlockSize: 10,
    localeText: { noRowsToShow: 'No Data Found' },
  };

  function cellStyle(params) {
    if (parseInt(params.value) < parseInt(params.data.Actual_IRR).toFixed(2)) {
      return { backgroundColor: '#ABD6DF' };
    } else if (
      parseInt(params.value) < parseInt(params.data.Prev_Planned_IRR.toFixed(2))
    ) {
      return { backgroundColor: '#7F6084' };
    } else if (
      parseInt(params.value) < parseInt(params.data.Actual_ROTA.toFixed(2))
    ) {
      return { backgroundColor: '#FF8533' };
    } else return {};
  }

  const columnDefs = [
    {
      headerName: 'Segment',
      field: 'Segment',
      suppressMenu: true,
    },
    {
      headerName: 'Asset Type',
      field: 'NewOrUsed',
      suppressMenu: true,
    },
    {
      headerName: 'Make',
      field: 'Make',
      suppressMenu: true,
      wrapText: true,
    },
    {
      headerName: 'Category',
      field: 'Category',
      suppressMenu: true,
      wrapText: true,
    },
    {
      headerName: 'Actual Volume(Units)',
      field: 'Actual_Volume',
      suppressMenu: true,
    },
    {
      headerName: 'Planned Volume(Units)',
      field: 'Planned_Volume',
      suppressMenu: true,
    },
    {
      headerName: 'Actual Value(in Lakhs)',
      field: 'Actual_Value',
      suppressMenu: true,
    },
    {
      headerName: 'Planned Value(in Lakhs)',
      field: 'Planned_Value',
      suppressMenu: true,
    },
    {
      headerName: 'Actual IRR(%)',
      field: 'Actual_IRR',
      suppressMenu: true,
    },
    {
      headerName: 'Planned IRR(%)',
      field: 'Planned_IRR',
      suppressMenu: true,
      cellStyle: cellStyle,
    },
    {
      headerName: 'Actual NCL(%)',
      field: 'Actual_NCL',
      suppressMenu: true,
    },
    {
      headerName: 'Book NCL(%)',
      field: 'Book_NCL/Avg_Assets',
      suppressMenu: true,
    },
    {
      headerName: 'Actual ROTA(%)',
      field: 'Actual_ROTA',
      suppressMenu: true,
    },
    {
      headerName: 'Book ROTA(%)',
      field: 'Book_PBT-ROTA',
      suppressMenu: true,
    },
    {
      headerName: 'Book OPEX(%)',
      field: 'Book_Overall_Opex',
      suppressMenu: true,
    },
    {
      headerName: 'Book OI(%)',
      field: 'Book_OI_AvgAssets',
      suppressMenu: true,
    },
  ];

  const onGridReady = (params) => {
    gridApi = params.api;
    gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
  };

  const onPageSizeChanged = (newPageSize) => {
    gridApi.paginationSetPageSize(Number(newPageSize));
  };

  const handleGridSearch = (value) => {
    gridApi.setQuickFilter(value);
  };

  return (
    <>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        style={{ padding: '0 20px' }}
      >
        <Grid item>
          {/* <PageSizeSelect onValueChange={onPageSizeChanged} /> */}
        </Grid>
        <Grid item>
          <GridSearchBox onValueChange={handleGridSearch} />
        </Grid>
      </Grid>
      <div
        className="ag-theme-alpine"
        style={{ height: 500, padding: 20, paddingBottom: 30 }}
      >
        <AgGridReact
          columnDefs={columnDefs}
          rowData={details}
          gridOptions={gridOptions}
          onGridReady={onGridReady}
        />
      </div>
    </>
  );
};

export default Summary;
