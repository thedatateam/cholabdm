import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
}));

const PageSizeSelect = ({
  items = [
    { text: '10', value: 10 },
    { text: '25', value: 25 },
    { text: '50', value: 50 },
    { text: '100', value: 100 },
  ],
  onValueChange,
}) => {
  const classes = useStyles();

  function handleChange(event) {
    const { value } = event.target;
    onValueChange(value);
  }

  return (
    <>
      <FormControl className={classes.formControl}>
        <InputLabel id="size-select-label">Show Entries</InputLabel>
        <Select
          labelId="size-select-label"
          id="size-select"
          defaultValue={items.length > 0 ? items[0].value : ''}
          onChange={handleChange}
        >
          {items.map((el) => {
            return (
              <MenuItem value={el.value} key={el.value}>
                {el.text}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </>
  );
};

export default PageSizeSelect;
