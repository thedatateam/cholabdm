import React from 'react';
import Grid from '@material-ui/core/Grid';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import PageSizeSelect from './PageSizeSelect';
import GridSearchBox from './GridSearchBox';

const MarginalROTA = ({ details }) => {
  let gridApi;
  let gridColumnApi;

  const gridOptions = {
    defaultColDef: {
      resizable: true,
      sortable: true,
    },
    suppressHorizontalScroll: false,
    paginationPageSize: 10,
    cacheBlockSize: 10,
    localeText: { noRowsToShow: 'No Data Found' },
  };

  function cellStyle(params) {
    if (parseInt(params.value) < parseInt(params.data.Actual_IRR).toFixed(2)) {
      return { backgroundColor: '#ABD6DF' };
    } else if (
      parseInt(params.value) < parseInt(params.data.Prev_Planned_IRR.toFixed(2))
    ) {
      return { backgroundColor: '#7F6084' };
    } else if (
      parseInt(params.value) < parseInt(params.data.Actual_ROTA.toFixed(2))
    ) {
      return { backgroundColor: '#FF8533' };
    } else return {};
  }

  const columnDefs = [
    {
      headerName: 'Segment',
      field: 'Segment',
      suppressMenu: true,
    },
    {
      headerName: 'Asset Type',
      field: 'NewOrUsed',
      suppressMenu: true,
    },
    {
      headerName: 'Make',
      field: 'Make',
      suppressMenu: true,
      wrapText: true,
    },
    {
      headerName: 'Category',
      field: 'Category',
      suppressMenu: true,
      wrapText: true,
    },
    {
      headerName: 'Marginal IRR(%)',
      field: 'Marginal_Int_Inc/Avg_Assets',
      suppressMenu: true,
      cellStyle: cellStyle,
    },
    {
      headerName: 'Marginal FI(%)',
      field: 'Marginal_FI_AvgAssets',
      suppressMenu: true,
    },
    {
      headerName: 'Marginal Insurance Income(%)',
      field: 'Marginal_Ins_AvgAssets',
      suppressMenu: true,
    },
    {
      headerName: 'Marginal OI(%)',
      field: 'OI',
      suppressMenu: true,
    },
    {
      headerName: 'Marginal COF(%)',
      field: 'COF',
      suppressMenu: true,
    },
    {
      headerName: 'Marginal NIM(%)',
      field: 'Marginal_NIM/Avg_Assets',
      suppressMenu: true,
    },
    {
      headerName: 'Marginal OPEX(%)',
      field: 'Marginal_Overall_Opex',
      suppressMenu: true,
    },
    {
      headerName: 'Marginal NCL(%)',
      field: 'Marginal_NCL/Avg_Assets',
      suppressMenu: true,
    },
    {
      headerName: 'Marginal ROTA(%)',
      field: 'Marginal_PBT-ROTA',
      suppressMenu: true,
    },
  ];

  const onGridReady = (params) => {
    gridApi = params.api;
    gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
  };

  const onPageSizeChanged = (newPageSize) => {
    gridApi.paginationSetPageSize(Number(newPageSize));
  };

  const handleGridSearch = (value) => {
    gridApi.setQuickFilter(value);
  };

  return (
    <>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        style={{ padding: '0 20px' }}
      >
        <Grid item>
          {/* <PageSizeSelect onValueChange={onPageSizeChanged} /> */}
        </Grid>
        <Grid item>
          <GridSearchBox onValueChange={handleGridSearch} />
        </Grid>
      </Grid>
      <div
        className="ag-theme-alpine"
        style={{ height: 500, padding: 20, paddingBottom: 30 }}
      >
        <AgGridReact
          columnDefs={columnDefs}
          rowData={details}
          gridOptions={gridOptions}
          onGridReady={onGridReady}
        />
      </div>
    </>
  );
};

export default MarginalROTA;
