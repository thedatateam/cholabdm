import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import P from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 20,
  },
}));

const Pagination = ({ count = 0, page = 1, pageChange, ...props }) => {
  const classes = useStyles();
  const handleChange = (event, value) => {
    pageChange(value);
  };

  return (
    <div className={classes.root}>
      <P
        count={count}
        page={page}
        onChange={handleChange}
        variant="outlined"
        shape="rounded"
        color="primary"
        {...props}
      />
    </div>
  );
};

export default Pagination;
