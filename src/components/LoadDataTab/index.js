import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import {adminEndpoint} from '../../config';
const host = adminEndpoint.host;


const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
}));

const LoadDataTab = () => {
  const classes = useStyles();
  const [type, setType] = React.useState('');
  const [file, setFile] = React.useState(null);
  const fileInputRef = React.useRef(null);
  const [open, setOpen] = React.useState(false);
  const [msg, setMsg] = React.useState(null);
  const [severity, setSeverity] = React.useState('error');
  const [status, setStatus] = React.useState('IDLE');

  const handleChange = (event) => {
    setType(event.target.value);
  };

  const handleSubmit = () => {
    // validate fields
    if (!file || !type) {
      setMsg('Both fields are required!');
      setOpen(true);
      return;
    }
    setStatus('PENDING');
    // create form data
    const formData = new FormData();
    formData.append('collection', type);
    formData.append('csv_file', file, 'data.csv');
    fetch(`${host}/cholaBDM/FileUpload/`, {
      method: 'POST',
      body: formData,
    })
      .then((res) => {
        if (res.status === 200) {
          return res.json();
        } else {
          throw new Error('Could not save Data! Try again later.');
        }
      })
      .then(() => {
        setOpen(true);
        setStatus('SUCCESS');
        setSeverity('success');
        setMsg('Data saved successfully.');
      })
      .catch((err) => {
        setOpen(true);
        setSeverity('error');
        setMsg(err.message || 'Could not save Data! Try again later.');
        setStatus('ERROR');
      })
      .finally(() => {
        setFile(null);
        setType('');
        const el = fileInputRef.current.children[0];
        el.files = null;
        el.value = '';
      });
  };

  const readFile = (event) => {
    setFile(event.target.files[0]);
  };

  const close = () => {
    setOpen(false);
    setMsg(null);
  };

  return (
    <>
      <Grid
        container
        direction="column"
        style={{ padding: '0 20px 20px' }}
        spacing={2}
      >
        <Grid item>
          <Collapse in={open}>
            <Alert
              severity={severity}
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={close}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
            >
              {msg}
            </Alert>
          </Collapse>
        </Grid>
        <Grid item>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Type of Data</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={type}
              onChange={handleChange}
            >
              <MenuItem value="branchstatus">Branch Status</MenuItem>
              <MenuItem value="mlr">MLR Data</MenuItem>
              <MenuItem value="overalldata">Overall Data</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item>
          <FormControl className={classes.formControl}>
            <Input
              id="standard-basic"
              type="file"
              inputProps={{ accept: '.csv' }}
              onChange={readFile}
              ref={fileInputRef}
            />
          </FormControl>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            style={{textTransform: 'none'}}
            onClick={handleSubmit}
            disabled={status === 'PENDING'}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default LoadDataTab;
