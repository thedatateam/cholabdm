import React from "react";
import {
  Grid,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Snackbar,
  Button,
  Paper
} from "@material-ui/core";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#0275d8',
      color: 'white',
      borderLeft:'inset'
    },
    body: {
      fontSize: 12,
    },
  }))(TableCell);
  const styles = theme => ({
    table: {
      minWidth: 700,
      padding:'5px'
    },
  });
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

 class ZoneManagerFlow extends React.Component {
 
  componentDidMount() {
  };


  render() {
const { classes,zoneData} = this.props;
    return (
     <div> Zone Level Summary
     
     <TableContainer component={Paper} style={{marginTop:'10px'}}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell >Metric</StyledTableCell>
            <StyledTableCell align="center" >Actual</StyledTableCell>
            <StyledTableCell align="center" >Planned</StyledTableCell>
            <StyledTableCell  align="center">Difference</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            <StyledTableRow key={'1'}>
            <StyledTableCell>Closing Asset(In Lakhs)</StyledTableCell>
            <StyledTableCell  align="center">{zoneData?zoneData.Closing_Assets && zoneData.Closing_Assets.toFixed(2):null}</StyledTableCell>
            <StyledTableCell align="center" >{zoneData?zoneData.Bud_Closing_Assets  && zoneData.Bud_Closing_Assets.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{zoneData?zoneData.AssetDiff  && parseInt(zoneData.AssetDiff).toFixed(2):null}</StyledTableCell>
            </StyledTableRow>
            <StyledTableRow key={'2'}>
             <StyledTableCell >ROTA (%)</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.Actual_ROTA  && zoneData.Actual_ROTA.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.Book_PBT_ROTA  && zoneData.Book_PBT_ROTA.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.ROTADiff  && zoneData.ROTADiff.toFixed(2):null}</StyledTableCell>
             </StyledTableRow>
             <StyledTableRow key={'3'}>
            <StyledTableCell >NCL (%)</StyledTableCell>
            <StyledTableCell  align="center">{zoneData?zoneData.Actual_NCL  && zoneData.Actual_NCL.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{zoneData?zoneData.Budget_NCL  && zoneData.Budget_NCL.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{zoneData?zoneData.NCLDiff  && zoneData.NCLDiff.toFixed(2):null}</StyledTableCell>
            </StyledTableRow>
             <StyledTableRow key={'4'}>
             <StyledTableCell >OPEX (%)</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.OPEX  && zoneData.OPEX.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.Budget_OPEX  && zoneData.Budget_OPEX.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.OPEXDiff  && parseInt(zoneData.OPEXDiff).toFixed(2):null}</StyledTableCell>
             </StyledTableRow>
             <StyledTableRow key={'5'}>
             <StyledTableCell >Yield (%)</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.Actual_IRR && zoneData.Actual_IRR.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.Planned_IRR  && zoneData.Planned_IRR.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.IRRDiff  && zoneData.IRRDiff.toFixed(2):null}</StyledTableCell>
              </StyledTableRow>
             <StyledTableRow key={'6'}>
             <StyledTableCell >Cross Sell (In Lakhs)</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.Actual_Cross_sell_income  && zoneData.Actual_Cross_sell_income.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.Planned_Cross_sell_income  && zoneData.Planned_Cross_sell_income.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{zoneData?zoneData.CrossSellDiff  && zoneData.CrossSellDiff.toFixed(2):null}</StyledTableCell>
 
             </StyledTableRow>
        </TableBody>
      </Table>
    </TableContainer>
     
     
     </div>
    );
  }
}
export default withStyles(styles)(ZoneManagerFlow);

