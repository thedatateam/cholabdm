import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { formatNumbers } from '../../utils';
import {
  getRMAreaLevelSummary,
  getZMAreaLevelSummary,
} from '../../actions/areaLevelSummary';

const useStyles = makeStyles({
  formControl: {
    margin: '10px',
    minWidth: 200,
  },
  ctaBtn: {
    backgroundColor: '#0275d8',
    margin: '10px 0',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#0275d8',
    },
  },
});

const AreaLevelSummary = () => {
  let gridApi;
  let gridColumnApi;
  const classes = useStyles();
  const areaLevelSummary = useSelector((state) => state.areaLevelSummary);
  const dispatch = useDispatch();
  const [rowData, setRowData] = useState([]);
  const [segment, setSegment] = useState('default');

  const gridOptions = {
    defaultColDef: {
      resizable: true,
      sortable: true,
    },
    suppressHorizontalScroll: false,
    localeText: { noRowsToShow: 'No Data Found' },
  };

  const columnDefs = [
    {
      headerName: 'Area',
      field: 'Area',
      suppressMenu: true,
    },
    {
      headerName: 'Segment',
      field: 'Segment',
      suppressMenu: true,
    },
    {
      headerName: 'Asset Type',
      field: 'NewOrUsed',
      suppressMenu: true,
    },
    {
      headerName: 'Actual Rota(%)',
      field: 'Actual_ROTA',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned Rota(%)',
      field: 'Planned_ROTA',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference Rota(%)',
      field: 'ROTADiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual NCL(%)',
      field: 'Actual_NCL',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned NCL(%)',
      field: 'Planned_NCL',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference NCL(%)',
      field: 'NCLDiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual IRR(%)',
      field: 'Actual_IRR',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned IRR(%)',
      field: 'Planned_IRR',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference IRR(%)',
      field: 'IRRDiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual Value(in Lakhs)',
      field: 'Actual_Value',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned Value(in Lakhs)',
      field: 'Planned_Value',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Bud Clossing Asset(in Lakhs)',
      field: 'Bud_Closing_Assets',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual Clossing Asset(in Lakhs)',
      field: 'Closing_Assets',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
  ];

  useEffect(() => {
    const flowType = localStorage.getItem('type');
    if (flowType === 'Region') {
      dispatch(getRMAreaLevelSummary());
    } else if (flowType === 'Zone') {
      dispatch(getZMAreaLevelSummary());
    }
  }, []);

  useEffect(() => {
    if (areaLevelSummary.statusRM === 'SUCCESS') {
      setRowData(areaLevelSummary.dataRM);
    } else if (areaLevelSummary.statusZM === 'SUCCESS') {
      setRowData(areaLevelSummary.dataZM);
    }
  }, [areaLevelSummary]);

  const onGridReady = (params) => {
    // console.log(params);
    gridApi = params.api;
    gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
  };

  return (
    <>
      <Grid container spacing={3} direction="column">
        <Grid item lg={12} style={{ textAlign: 'center' }}>
          Area level Summary
        </Grid>
        <Grid item lg={12}>
          <FormControl className={classes.formControl}>
            <InputLabel id="segment">Segment</InputLabel>
            <Select
              labelId="segment"
              id="segment"
              name="segment"
              value={segment}
              onChange={(e) => setSegment(e.target.value)}
            >
              <MenuItem value={'default'}>default</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item lg={12}>
          {(areaLevelSummary.statusRM === 'PENDING' ||
            areaLevelSummary.statusZM === 'PENDING') && <>Loading ... </>}
          {(areaLevelSummary.statusRM === 'SUCCESS' ||
            areaLevelSummary.statusZM === 'SUCCESS') && (
            <div
              className="ag-theme-alpine"
              style={{
                height: '520px',
                width: '100%',
              }}
            >
              <AgGridReact
                rowData={rowData}
                columnDefs={columnDefs}
                pagination={false}
                onGridReady={onGridReady}
                gridOptions={gridOptions}
              />
            </div>
          )}
        </Grid>
      </Grid>
    </>
  );
};

export default AreaLevelSummary;
