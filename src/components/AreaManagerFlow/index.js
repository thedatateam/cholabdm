import React from "react";
import {
  Grid,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Snackbar,
  Button,
  Paper
} from "@material-ui/core";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#0275d8',
      color: 'white',
      borderLeft:'inset'
    },
    body: {
      fontSize: 12,
    },
  }))(TableCell);
  const styles = theme => ({
    table: {
      minWidth: 700,
      padding:'5px'
    },
  });
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

 class AreaManagerFlow extends React.Component {
 
  componentDidMount() {
  };


  render() {
const { classes,areaData} = this.props;

    return (
     <div> Area Level Summary
     
     <TableContainer component={Paper} style={{marginTop:'10px'}}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell >Metric</StyledTableCell>
            <StyledTableCell align="center" >Actual</StyledTableCell>
            <StyledTableCell align="center" >Planned</StyledTableCell>
            <StyledTableCell  align="center">Difference</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            <StyledTableRow key={'1'}>
            <StyledTableCell>Closing Asset(In Lakhs)</StyledTableCell>
            <StyledTableCell  align="center">{areaData?areaData.Closing_Assets && areaData.Closing_Assets.toFixed(2):null}</StyledTableCell>
            <StyledTableCell align="center" >{areaData?areaData.Bud_Closing_Assets  && areaData.Bud_Closing_Assets.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{areaData?areaData.AssetDiff  && parseInt(areaData.AssetDiff).toFixed(2):null}</StyledTableCell>
            </StyledTableRow>
            <StyledTableRow key={'2'}>
             <StyledTableCell >ROTA (%)</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.Actual_ROTA  && areaData.Actual_ROTA.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.Book_PBT_ROTA  && areaData.Book_PBT_ROTA.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.ROTADiff  && areaData.ROTADiff.toFixed(2):null}</StyledTableCell>
             </StyledTableRow>
             <StyledTableRow key={'3'}>
            <StyledTableCell >NCL (%)</StyledTableCell>
            <StyledTableCell  align="center">{areaData?areaData.Actual_NCL  && areaData.Actual_NCL.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{areaData?areaData.Budget_NCL  && areaData.Budget_NCL.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{areaData?areaData.NCLDiff  && areaData.NCLDiff.toFixed(2):null}</StyledTableCell>
            </StyledTableRow>
             <StyledTableRow key={'4'}>
             <StyledTableCell >OPEX (%)</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.OPEX  && areaData.OPEX.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.Budget_OPEX  && areaData.Budget_OPEX.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.OPEXDiff  && parseInt(areaData.OPEXDiff).toFixed(2):null}</StyledTableCell>
             </StyledTableRow>
             <StyledTableRow key={'5'}>
             <StyledTableCell >Yield (%)</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.Actual_IRR && areaData.Actual_IRR.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.Planned_IRR  && areaData.Planned_IRR.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.IRRDiff  && areaData.IRRDiff.toFixed(2):null}</StyledTableCell>
              </StyledTableRow>
             <StyledTableRow key={'6'}>
             <StyledTableCell >Cross Sell (In Lakhs)</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.Actual_Cross_sell_income  && areaData.Actual_Cross_sell_income.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.Planned_Cross_sell_income  && areaData.Planned_Cross_sell_income.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{areaData?areaData.CrossSellDiff  && areaData.CrossSellDiff.toFixed(2):null}</StyledTableCell>
 
             </StyledTableRow>
        </TableBody>
      </Table>
    </TableContainer>
     
     
     </div>
    );
  }
}
export default withStyles(styles)(AreaManagerFlow);

