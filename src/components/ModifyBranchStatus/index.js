import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '@material-ui/icons/Close';
import { FormatStrikethroughSharp } from '@material-ui/icons';
import {adminEndpoint} from '../../config'
const host = adminEndpoint.host;


const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
}));

const ModifyBranchStatus = () => {
  const classes = useStyles();
  const [type, setType] = React.useState('');
  const [typeZoneValue, setTypeZoneValue] = React.useState('');

  const [period, setPeriod] = React.useState('');
  const [typeValue, setTypeValue] = React.useState('');
  const [file, setFile] = React.useState(null);
  const fileInputRef = React.useRef(null);
  const [open, setOpen] = React.useState(false);
  const [resetValue, setResetValue] = React.useState(false);
  const [msg, setMsg] = React.useState(null);
  const [severity, setSeverity] = React.useState('error');
  const [status, setStatus] = React.useState('IDLE');
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];
const d = new Date();
var n = monthNames[d.getMonth()]
  const handleChange = (event) => {
    setType(event.target.value);
  };
  const handleZoneChange = (event) => {
    setTypeZoneValue(event.target.value);
 

  };
  const handleResetChange = (event) => {
    setResetValue(event.target.value);
 
  };

  const handleSubmit = () => {
    // validate fields
    if (!file && !type) {
      setMsg('Both fields are required!');
      setOpen(true);
      return;
    }
    setStatus('PENDING');
    // create form data
    let name='';
    if(typeZoneValue && typeZoneValue.length){
     name= typeZoneValue}
            else{  name=typeValue}
            
    let formData ={
   'Period':n,
   'ReadOnly':resetValue==="True"?true:false,
   'Name':name
 
    }

    fetch(`${host}/cholaBDM/updateBranchStatus`, {
        headers: {
            "Content-Type": "application/json",
        
          },
      method: 'PUT',
      body:  JSON.stringify(formData)
    })
      .then((res) => {
        if (res.status === 200) {
          return res.json();
        } else {
          throw new Error('Could not Change Status! Try again later.');
        }
      })
      .then(() => {
        setOpen(true);
        setStatus('SUCCESS');
        setSeverity('success');
        setMsg('Status Changed Successfully.');
      })
      .catch((err) => {
        setOpen(true);
        setSeverity('error');
        setMsg(err.message || 'Could not Change Status!  Try again later.');
        setStatus('ERROR');
      })
      .finally(() => {
        setFile(null);
        setType('');
        setTypeZoneValue('');
        setResetValue('');
        setType('');

        // const el = fileInputRef.current.children[0];
        // el.files = null;
        // el.value = '';
      });
  };

  const readFile = (event) => {
    setFile(event.target.files[0]);
  };

  const close = () => {
    setOpen(false);
    setMsg(null);
  };

  return (
    <>
      <Grid
        container
        direction="column"
        style={{ padding: '0 20px 20px' }}
        spacing={2}
      >
    <Grid item>
          <Collapse in={open}>
            <Alert
              severity={severity}
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={close}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
            >
              {msg}
            </Alert>
          </Collapse>
        </Grid>
        <Grid item>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label"> Select Type </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={type}
              onChange={handleChange}
            >
              <MenuItem value="Region">Region</MenuItem>
              <MenuItem value="Zone">Zone</MenuItem>
              <MenuItem value="Area">Area</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        {type && type.length?
        <>
        {type==='Zone'?
        <Grid item>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label"> Select Zone </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={typeZoneValue}
              onChange={handleZoneChange}
            >
              <MenuItem value="East">East</MenuItem>
              <MenuItem value="West">West</MenuItem>
              <MenuItem value="North">North</MenuItem>
              <MenuItem value="South">South</MenuItem>

            </Select>
          </FormControl>
        </Grid>
     :
     <Grid item>
     <FormControl className={classes.formControl}>
     <TextField
                id="outlined-basic"
                label={type}
                variant="outlined"
                placeholder={type}
                fullWidth
                value={typeValue}
                onChange={(e) => setTypeValue(e.target.value)}
              />
         </FormControl>
         </Grid>}
         </>:null}
         <Grid item>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label"> Reset Status </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={resetValue}
              onChange={handleResetChange}
            >
              <MenuItem value="True">Yes</MenuItem>
              <MenuItem value="False">No</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit}
            disabled={status === 'PENDING'}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default ModifyBranchStatus;
