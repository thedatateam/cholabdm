import React from 'react';
import { useHistory } from 'react-router-dom';

const SegmentCellRenderer = ({ value }) =>  {
  const history = useHistory();

  function goToMainSheet() {
    localStorage.setItem("segment", value)
    history.push('/mainSheet');
  }

  return (
    <span>
      <button className="segment-btn" onClick={goToMainSheet}>
        {value}
      </button>
    </span>
  );
};

export default SegmentCellRenderer;
