import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import {
  getAssetTypeView,
  getRMAssetTypeView,
  getZMAssetTypeView,
} from '../../actions/assetTypeView';
import SegmentCellRenderer from './SegmentCellRenderer';
import { formatNumbers } from '../../utils';
import './assetType.scss';

const useStyles = makeStyles({
  formControl: {
    margin: '10px',
    minWidth: 200,
  },
  ctaBtn: {
    backgroundColor: '#0275d8',
    margin: '10px 0',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#0275d8',
    },
  },
});

const AssetTypeView = () => {
  let gridApi;
  let gridColumnApi;
  const classes = useStyles();
  const assetTypeData = useSelector((state) => state.assetTypeView);
  const dispatch = useDispatch();
  const [type, setType] = React.useState('all');
  const [rowData, setRowData] = React.useState([]);

  const gridOptions = {
    defaultColDef: {
      resizable: true,
      sortable: true,
    },
    suppressHorizontalScroll: false,
    localeText: { noRowsToShow: 'No Data Found' },
  };

  const columnDefs = [
    {
      headerName: 'Segment',
      field: 'Segment',
      suppressMenu: true,
      width: 200,
      cellRenderer: 'Segment',
    },
    {
      headerName: 'Actual Rota(%)',
      field: 'Actual_ROTA',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned Rota(%)',
      field: 'Planned_ROTA',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference Rota(%)',
      field: 'ROTADiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual NCL(%)',
      field: 'Actual_NCL',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned NCL(%)',
      field: 'Planned_NCL',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference NCL(%)',
      field: 'NCLDiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual IRR(%)',
      field: 'Actual_IRR',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned IRR(%)',
      field: 'Planned_IRR',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Difference IRR(%)',
      field: 'IRRDiff',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual Value(in Lakhs)',
      field: 'Actual_Value',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Planned Value(in Lakhs)',
      field: 'Planned_Value',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Bud Clossing Asset(in Lakhs)',
      suppressMenu: true,
      field: 'Bud_Closing_Assets',
      valueFormatter: formatNumbers,
    },
    {
      headerName: 'Actual Clossing Asset(in Lakhs)',
      field: 'Closing_Assets',
      suppressMenu: true,
      valueFormatter: formatNumbers,
    },
  ];

  React.useEffect(() => {
    const flowType = localStorage.getItem('type');
    if (flowType === 'Area') {
      dispatch(getAssetTypeView());
    } else if (flowType === 'Region') {
      dispatch(getRMAssetTypeView());
    } else if (flowType === 'Zone') {
      dispatch(getZMAssetTypeView());
    }
  }, []);

  React.useEffect(() => {
    if (assetTypeData.status === 'SUCCESS') {
      setRowData(assetTypeData.allData);
    }

    if (assetTypeData.rmStatus === 'SUCCESS') {
      setRowData(assetTypeData.allRMData);
    }

    if (assetTypeData.zmStatus === 'SUCCESS') {
      setRowData(assetTypeData.allZMData);
    }
  }, [assetTypeData]);

  React.useEffect(() => {
    setRowData(handleTypeChange(type));
  }, [type]);

  function handleTypeChange(type) {
    const flowType = localStorage.getItem('type');
    if (flowType === 'Area') {
      const key = `${type}Data`;
      return assetTypeData[key];
    } else if (flowType === 'Region') {
      const key = `${type}RMData`;
      return assetTypeData[key];
    } else if (flowType === 'Zone') {
      const key = `${type}ZMData`;
      return assetTypeData[key];
    }
  }

  const onGridReady = (params) => {
    // console.log(params);
    gridApi = params.api;
    gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
  };

  return (
    <>
      <Grid container spacing={3} direction="column">
        <Grid item lg={12} style={{ textAlign: 'center' }}>
          {localStorage.getItem('type') === 'Area'
            ? 'Area Level Summary'
            : localStorage.getItem('type') === 'Region'
            ? 'Region Level Summary'
            : 'Zone Level Summary'}
        </Grid>
        <Grid item lg={12}>
          <FormControl className={classes.formControl}>
            <InputLabel id="assetType">Asset Type</InputLabel>
            <Select
              labelId="assetType"
              id="type"
              name="type"
              value={type}
              onChange={(e) => setType(e.target.value)}
            >
              <MenuItem value={'all'}>All</MenuItem>
              <MenuItem value={'new'}>New</MenuItem>
              <MenuItem value={'shubh'}>Shubh</MenuItem>
              <MenuItem value={'used'}>Used</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item lg={12}>
          {assetTypeData.error && (
            <div>Error Occured. Could not fetch data.</div>
          )}
          {(assetTypeData.status === 'SUCCESS' ||
            assetTypeData.rmStatus === 'SUCCESS' ||
            assetTypeData.zmStatus === 'SUCCESS') && (
            <div
              className="ag-theme-alpine"
              style={{
                height: '520px',
                width: '100%',
              }}
            >
              <AgGridReact
                frameworkComponents={{ Segment: SegmentCellRenderer }}
                rowData={rowData}
                columnDefs={columnDefs}
                pagination={false}
                onGridReady={onGridReady}
                gridOptions={gridOptions}
              />
            </div>
          )}
          {(assetTypeData.status === 'PENDING' ||
            assetTypeData.rmStatus === 'PENDING' ||
            assetTypeData.zmStatus === 'PENDING') && <>Loading ... </>}
        </Grid>
      </Grid>
    </>
  );
};

export default AssetTypeView;
