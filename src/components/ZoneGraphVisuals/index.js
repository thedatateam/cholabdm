import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//material-ui components
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Slide from '@material-ui/core/Slide';
import * as zoneManagerActions from "../../actions/zoneLevelFlow";
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import CholaLogo from '../../assets/images/cholaLogo.png';
import { Bar } from 'react-chartjs-2';
import * as areaManagerActions from "../../actions/areaManagerFlow";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

//other libraries
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import classNames from 'classnames';
import { SnackbarProvider, withSnackbar } from 'notistack';
function Transition(props) {
    return <Slide direction="up" {...props} />;
}
const styles = theme => ({

    root: {
        height: '100%',
    },
    agheader: {
        backgroundColors: "green"
    },
    formControl: {
        margin: '5px 0',
        width: '20%',
        Align: 'center',
      },
      tabSpace:{
        paddingLeft:'100px',
      }

});

class ZoneGraphVisual extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: '',
            area: '',
            month: '',
            role: '',
            allGraphData: [],
            segArray: [],
            graphFilter: [],
            plannedNCL: [],
            plannedVol: [],
            plannedIRR: [],
            plannedValue: [],
            plannedROTA: [],
            dataType:'',
            dataTypeValue:'',
            dropDownData:[]
        }

    };
    componentDidMount() {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        const d = new Date();
        var n = monthNames[d.getMonth()]
        this.setState({ month: n });
        var year = new Date().getFullYear().toString().substr(-2);
        let nextYear = parseInt(year) + 1;
        let role = localStorage.getItem('role');
        this.setState({ role: role })
        let area = localStorage.getItem('typeValue');
        this.setState({ area: area });
        let type = localStorage.getItem('type');
        this.props.actions.getGraphData(area, n);
    }
    handleNext = () => {
        if(this.state.dataType==='Make')
        this.props.actions.getCustomerSegmentGraphData('Zone',this.state.area,this.state.dataTypeValue,this.state.month);
        else if(this.state.dataType==='Category')
        this.props.actions.getCustomerCategoryGraphData('Zone',this.state.area,this.state.dataTypeValue,this.state.month);

    }
    handleChange(prop, event) {
        this.setState({ [prop]: event.target.value });
        if (prop==='dataType')
        this.props.actions.getAllDropDown(event.target.value )
    };


    static getDerivedStateFromProps(nextProps, state, props) {
        var segArray= [];
        var plannedNCL= [];
         var plannedVol= [];
        var plannedIRR= [];
        var plannedValue= [];
        var plannedROTA= [];
        var data;
        if (nextProps.areaManager.getALLDropDownSuccess === true && nextProps.areaManager.responseData) {
            nextProps.areaManager.getALLDropDownSuccess = false;
            return {
                dropDownData: nextProps.areaManager.responseData.AreasInRegion
            }
          }
          if (nextProps.areaManager.getALLDropDownError === true && nextProps.areaManager.error) {
            nextProps.areaManager.getALLDropDownError = false;
            return {
                dropDownData: [],
            }
          }
          if (nextProps.areaManager. getCustomerSegmentGraphDataSuccess === true && nextProps.areaManager.responseGraphData) {

            nextProps.areaManager. getCustomerSegmentGraphDataSuccess = false;
             data = nextProps.areaManager.responseGraphData.overalldata;
           data.map((snapShot, idx) => {
            segArray.push(snapShot.Make)
           

           });
            data.map((snapShot, idx) => (
                plannedVol.push(snapShot.Planned_Volume)
            ));
            data.map((snapShot, idx) => (
                plannedNCL.push(snapShot.Planned_NCL)
            ));
            data.map((snapShot, idx) => (
                plannedROTA.push(snapShot.Planned_ROTA)
            ));
            data.map((snapShot, idx) => (
                plannedValue.push(snapShot.Planned_Value)
            ));
            data.map((snapShot, idx) => (
                plannedIRR.push(snapShot.Planned_IRR)
            ));
            return {
                // allGraphData: nextProps.assetTypeView.allData,
                segArray,
                plannedNCL,
                plannedVol,
                plannedIRR,
                plannedValue,
                plannedROTA
            }
          }
          if (nextProps.areaManager.getCustomerSegmentGraphDataError === true && nextProps.areaManager.getRoleErrorStatus) {
            nextProps.areaManager.getCustomerSegmentGraphDataError = false;
            return {
                graphFilter: [],
            }
          }
          if (nextProps.areaManager. getCustomerCategoryGraphDataSuccess === true && nextProps.areaManager.responseCategoryGraphData) {
            nextProps.areaManager. getCustomerCategoryGraphDataSuccess = false;
             data = nextProps.areaManager.responseCategoryGraphData.overalldata;
           data.map((snapShot, idx) => {
            
            segArray.push(snapShot.Category)
           
        
           });
            data.map((snapShot, idx) => (
                plannedVol.push(snapShot.Planned_Volume)
            ));
            data.map((snapShot, idx) => (
                plannedNCL.push(snapShot.Planned_NCL)
            ));
            data.map((snapShot, idx) => (
                plannedROTA.push(snapShot.Planned_ROTA)
            ));
            data.map((snapShot, idx) => (
                plannedValue.push(snapShot.Planned_Value)
            ));
            data.map((snapShot, idx) => (
                plannedIRR.push(snapShot.Planned_IRR)
            ));
            return {
                // allGraphData: nextProps.assetTypeView.allData,
                segArray,
                plannedNCL,
                plannedVol,
                plannedIRR,
                plannedValue,
                plannedROTA
            }
          }
          if (nextProps.areaManager.getCustomerCategoryGraphDataError === true && nextProps.areaManager.getRoleErrorStatus) {
            nextProps.areaManager.getCustomerCategoryGraphDataError = false;
            return {
                graphFilter: [],
            }
          }
        if (nextProps.zoneManager && nextProps.zoneManager.graphDataSuccess === true && nextProps.zoneManager.response && nextProps.zoneManager.response.overalldata) {
             data = nextProps.zoneManager.response.overalldata;
            
            data.map((snapShot, idx) => (
                segArray.push(snapShot.Period)

            ));
            data.map((snapShot, idx) => (
                plannedVol.push(snapShot.Planned_Volume)
            ));
            data.map((snapShot, idx) => (
                plannedNCL.push(snapShot.Planned_NCL)
            ));
            data.map((snapShot, idx) => (
                plannedROTA.push(snapShot.Planned_ROTA)
            ));
            data.map((snapShot, idx) => (
                plannedValue.push(snapShot.Planned_Value)
            ));
            data.map((snapShot, idx) => (
                plannedIRR.push(snapShot.Planned_IRR)
            ));
            nextProps.zoneManager.getGraphDataSuccess === false; 
            return {
                allGraphData: nextProps.zoneManager.response.overalldata,
                segArray,
            plannedNCL,
            plannedVol,
            plannedIRR,
            plannedValue,
            plannedROTA
            }
        }
        // if (nextProps.areaManager.getFinalizedRateError === true && nextProps.areaManager.getRoleErrorStatus) {
        //     nextProps.areaManager.getFinalizedRateError = false;
        //     return {
        //         finalizedRateData: [],
        //     }
        // }
      

        return null;
    }
    render() {
        const { classes } = this.props;
        const { plannedIRR, plannedValue, plannedROTA, plannedNCL, plannedVol, segArray } = this.state;    
        var plannedVolData = {
            labels: segArray,
            datasets: [
                {
                    labels: segArray,
                    data: plannedVol,
                    backgroundColor: ["rgba(255,100,102,0.7)", "rgb(0, 191, 255,0.7)", "rgb(0,255,128,0.7)", "rgb(51,0,51,0.7)", "rgba(171,214,223,0.7)", "rgba(255,241,51,0.7)", "rgba(255,131,51,0.7)", "rgba(173,255,47,0.7)", "rgba(255,51,71,0.7)", "rgb(102, 153, 0, 0.7)", "rgb(179, 102, 255, 0.7)", "rgb(140, 217, 179, 0.7)", "rgb(255, 77, 166, 0.7)", "rgb(255, 51, 51, 0.7)"],
                    borderColor: ["rgba(255,100,102,1)", "rgb(0, 191, 255,1)", "rgb(0,255,128,1)", "rgb(51,0,51,1)", "rgba(171,214,223,1)", "rgba(255,241,51,1)", "rgba(255,131,51,1)", "rgba(173,255,47,1)", "rgba(255,51,71,1)", "rgb(102, 153, 0, 1)", "rgb(179, 102, 255, 1)", "rgb(140, 217, 179, 1)", "rgb(255, 77, 166, 1)", "rgb(255, 51, 51, 1)"],
                    borderWidth: 2,
                },
            ],
        };

        var plannedVolOptions = {
            maintainAspectRatio: true,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                    },
                ],
            },
            responsive: true,
            plugins: {
                legend: {
                    position: 'right',
                    display: false,

                },
                title: {
                    display: true,
                    text: 'Disbursement Volume',
                },
            },
        };
            var plannedIRRData = {
            labels: segArray,
            datasets: [
                {
                    labels: segArray,
                    data: plannedIRR,
                    backgroundColor: ["rgba(255,100,102,0.7)", "rgb(0, 191, 255,0.7)", "rgb(0,255,128,0.7)", "rgb(51,0,51,0.7)", "rgba(171,214,223,0.7)", "rgba(255,241,51,0.7)", "rgba(255,131,51,0.7)", "rgba(173,255,47,0.7)", "rgba(255,51,71,0.7)", "rgb(102, 153, 0, 0.7)", "rgb(179, 102, 255, 0.7)", "rgb(140, 217, 179, 0.7)", "rgb(255, 77, 166, 0.7)", "rgb(255, 51, 51, 0.7)"],
                    borderColor: ["rgba(255,100,102,1)", "rgb(0, 191, 255,1)", "rgb(0,255,128,1)", "rgb(51,0,51,1)", "rgba(171,214,223,1)", "rgba(255,241,51,1)", "rgba(255,131,51,1)", "rgba(173,255,47,1)", "rgba(255,51,71,1)", "rgb(102, 153, 0, 1)", "rgb(179, 102, 255, 1)", "rgb(140, 217, 179, 1)", "rgb(255, 77, 166, 1)", "rgb(255, 51, 51, 1)"],
                    borderWidth: 2,
                },
            ],
        };

        var plannedIRROptions = {
            maintainAspectRatio: true,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                    },
                ],
            },
            responsive: true,
            plugins: {
                legend: {
                    position: 'right',
                    display: false,

                },
                title: {
                    display: true,
                    text: 'IRR',
                },
            },
        };
        var plannedNCLData = {
            labels: segArray,
            datasets: [
                {
                    labels: segArray,
                    data: plannedNCL,
                    backgroundColor: ["rgba(255,100,102,0.7)", "rgb(0, 191, 255,0.7)", "rgb(0,255,128,0.7)", "rgb(51,0,51,0.7)", "rgba(171,214,223,0.7)", "rgba(255,241,51,0.7)", "rgba(255,131,51,0.7)", "rgba(173,255,47,0.7)", "rgba(255,51,71,0.7)", "rgb(102, 153, 0, 0.7)", "rgb(179, 102, 255, 0.7)", "rgb(140, 217, 179, 0.7)", "rgb(255, 77, 166, 0.7)", "rgb(255, 51, 51, 0.7)"],
                    borderColor: ["rgba(255,100,102,1)", "rgb(0, 191, 255,1)", "rgb(0,255,128,1)", "rgb(51,0,51,1)", "rgba(171,214,223,1)", "rgba(255,241,51,1)", "rgba(255,131,51,1)", "rgba(173,255,47,1)", "rgba(255,51,71,1)", "rgb(102, 153, 0, 1)", "rgb(179, 102, 255, 1)", "rgb(140, 217, 179, 1)", "rgb(255, 77, 166, 1)", "rgb(255, 51, 51, 1)"],
                    borderWidth: 2,
                },
            ],
        };
        var plannedNCLOptions = {
            maintainAspectRatio: true,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                    },
                ],
            },
            responsive: true,
            plugins: {
                legend: {
                    position: 'right',
                    display: false,

                },
                title: {
                    display: true,
                    text: 'NCL',
                },
            },
        };
        var plannedValueData = {
            labels: segArray,
            datasets: [
                {
                    labels: segArray,
                    data: plannedValue,
                    backgroundColor: ["rgba(255,100,102,0.7)", "rgb(0, 191, 255,0.7)", "rgb(0,255,128,0.7)", "rgb(51,0,51,0.7)", "rgba(171,214,223,0.7)", "rgba(255,241,51,0.7)", "rgba(255,131,51,0.7)", "rgba(173,255,47,0.7)", "rgba(255,51,71,0.7)", "rgb(102, 153, 0, 0.7)", "rgb(179, 102, 255, 0.7)", "rgb(140, 217, 179, 0.7)", "rgb(255, 77, 166, 0.7)", "rgb(255, 51, 51, 0.7)"],
                    borderColor: ["rgba(255,100,102,1)", "rgb(0, 191, 255,1)", "rgb(0,255,128,1)", "rgb(51,0,51,1)", "rgba(171,214,223,1)", "rgba(255,241,51,1)", "rgba(255,131,51,1)", "rgba(173,255,47,1)", "rgba(255,51,71,1)", "rgb(102, 153, 0, 1)", "rgb(179, 102, 255, 1)", "rgb(140, 217, 179, 1)", "rgb(255, 77, 166, 1)", "rgb(255, 51, 51, 1)"],
                    borderWidth: 2,
                },
            ],
        };
        var plannedValueOptions = {
            maintainAspectRatio: true,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                    },
                ],
            },
            responsive: true,
            plugins: {
                legend: {
                    position: 'right',
                    display: false,

                },
                title: {
                    display: true,
                    text: 'Disbursement value',
                },
            },
        };
        var plannedROTAData = {
            labels: segArray,
            datasets: [
                {
                    labels: segArray,
                    data: plannedROTA,
                    backgroundColor: ["rgba(255,100,102,0.7)", "rgb(0, 191, 255,0.7)", "rgb(0,255,128,0.7)", "rgb(51,0,51,0.7)", "rgba(171,214,223,0.7)", "rgba(255,241,51,0.7)", "rgba(255,131,51,0.7)", "rgba(173,255,47,0.7)", "rgba(255,51,71,0.7)", "rgb(102, 153, 0, 0.7)", "rgb(179, 102, 255, 0.7)", "rgb(140, 217, 179, 0.7)", "rgb(255, 77, 166, 0.7)", "rgb(255, 51, 51, 0.7)"],
                    borderColor: ["rgba(255,100,102,1)", "rgb(0, 191, 255,1)", "rgb(0,255,128,1)", "rgb(51,0,51,1)", "rgba(171,214,223,1)", "rgba(255,241,51,1)", "rgba(255,131,51,1)", "rgba(173,255,47,1)", "rgba(255,51,71,1)", "rgb(102, 153, 0, 1)", "rgb(179, 102, 255, 1)", "rgb(140, 217, 179, 1)", "rgb(255, 77, 166, 1)", "rgb(255, 51, 51, 1)"],
                    borderWidth: 2,
                },
            ],
        };
        var plannedROTAOptions = {
            maintainAspectRatio: true,
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                    },
                ],
            },
            responsive: true,
            plugins: {
                legend: {
                    position: 'right',
                    display: false,
                },
                title: {
                    display: true,
                    text: 'Planned ROTA',
                },
            },
        };
        return (
            <div>
                <Grid container>
                    <div style={{ height: '700px', width: '700px',margin:30,marginTop:'50px' }}>
                    Filters<br></br>
                         <span className={classes.tab}></span>
             <FormControl className={classes.formControl}>
            <InputLabel id="assetType">Select Type</InputLabel>
            <Select
              labelId="Select type"              
              id="type"
              name="type"
           value={this.state.dataType} 
             onChange={(e) => this.handleChange('dataType', e)}
             >    
             <MenuItem value={'Make'}>MAKE</MenuItem>
            <MenuItem value={'Category'}>CATEGORY</MenuItem>
         
            </Select>
          </FormControl> 
          <span className={classes.tabSpace}></span>
             <FormControl className={classes.formControl}>
            <InputLabel id="assetType">Select Segment</InputLabel>
            <Select
              labelId="Select Segment"              
              id="type"
              name="type"
           value={this.state.dataTypeValue} 
             onChange={(e) => this.handleChange('dataTypeValue', e)}
             >    
                {this.state.dropDownData.map(data => 
                    <MenuItem key={data} value={data}>{data}</MenuItem>
                 )}
           
            </Select>          </FormControl> 
          <Button variant="contained"    
                color="primary"
                onClick={this.handleNext}
                style={{ backgroundColor: '#0275d8',textTransform: 'none', marginRight:'6px',alignSelf:'center',width:'100px',margin:"20px"}}  >                          
               Show</Button>           
                    <Paper style={{ marginRight:'6px',alignSelf:'center',width:'1000px',margin:"20px"}}>   <Bar data={plannedIRRData} options={plannedIRROptions} style={{marginTop:'50px' }} /></Paper>  
                     <Paper style={{ marginRight:'6px',alignSelf:'center',width:'1000px',margin:"20px"}}> <Bar data={plannedVolData} options={plannedVolOptions} style={{marginTop:'50px' }}/></Paper>  
                     <Paper style={{ marginRight:'6px',alignSelf:'center',width:'1000px',margin:"20px"}}>  <Bar data={plannedNCLData} options={plannedNCLOptions}style={{marginTop:'50px' }} /></Paper>  
                     <Paper style={{ marginRight:'6px',alignSelf:'center',width:'1000px',margin:"20px"}}>   <Bar data={plannedROTAData} options={plannedROTAOptions} style={{marginTop:'50px' }}/></Paper>  
                     <Paper style={{ marginRight:'6px',alignSelf:'center',width:'1000px',margin:"20px"}}>   <Bar data={plannedValueData} options={plannedValueOptions} style={{marginTop:'50px' }}/></Paper>  

                    </div>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    zoneManager: state.zoneManager,
    areaManager: state.areaManager
});

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({ ...zoneManagerActions, ...areaManagerActions  }, dispatch)

    };
};

const WrappedZoneGraphVisual = withMobileDialog()(withStyles(styles)(withSnackbar(ZoneGraphVisual)));

class ZoneGraphVisualWithSnackBar extends React.Component {
    render() {
        return (
            <SnackbarProvider
                maxSnack={10}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                <WrappedZoneGraphVisual {...this.props} />
            </SnackbarProvider>
        );
    }
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ZoneGraphVisualWithSnackBar);
