import { Button } from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import { submitForApproval } from '../../actions/submitForApprovalZone';

const ZoneApproveButton = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [openSuccess, setOpenSuccess] = React.useState(false);
  const submitForApprovalZoneState = useSelector(
    (state) => state.submitForApprovalZone
  );

  React.useEffect(() => {
    if (submitForApprovalZoneState.status === 'SUCCESS') {
      setOpen(false);
      setOpenSuccess(true);
      localStorage.setItem('RMreworkStatus', 0);
      localStorage.setItem('zmSubmitVal', 1);
    }
  }, [submitForApprovalZoneState]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function handleSubmitApproval() {
    dispatch(submitForApproval());
  }

  function handleCloseSuccess() {
    setOpenSuccess(false);
  }
  return (
    <>
      <Button
        variant="contained"
        style={{ textTransform: 'none', backgroundColor: '#0275d8', margin: '10px', color: 'white' }}
        onClick={handleClickOpen}
      >
        Approve
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {'Are you sure to submit the data?'}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmitApproval} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openSuccess}
        onClose={handleCloseSuccess}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {'Approved Successfully.'}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseSuccess} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ZoneApproveButton;
