import React from "react";
import {
  Grid,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Snackbar,
  Button,
  Paper
} from "@material-ui/core";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#0275d8',
      color: 'white',
      borderLeft:'inset'
    },
    body: {
      fontSize: 12,
    },
  }))(TableCell);
  const styles = theme => ({
    table: {
      minWidth: 700,
      padding:'5px'
    },
  });
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

 class RegionManagerFlow extends React.Component {
 
  componentDidMount() {
  };


  render() {
const { classes,regionData} = this.props;
    return (
     <div> Region Level Summary
     
     <TableContainer component={Paper} style={{marginTop:'10px'}}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell >Metric</StyledTableCell>
            <StyledTableCell align="center" >Actual</StyledTableCell>
            <StyledTableCell align="center" >Planned</StyledTableCell>
            <StyledTableCell  align="center">Difference</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            <StyledTableRow key={'1'}>
            <StyledTableCell>Closing Asset(In Lakhs)</StyledTableCell>
            <StyledTableCell  align="center">{regionData?regionData.Closing_Assets && regionData.Closing_Assets.toFixed(2):null}</StyledTableCell>
            <StyledTableCell align="center" >{regionData?regionData.Bud_Closing_Assets  && regionData.Bud_Closing_Assets.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{regionData?regionData.AssetDiff  && parseInt(regionData.AssetDiff).toFixed(2):null}</StyledTableCell>
            </StyledTableRow>
            <StyledTableRow key={'2'}>
             <StyledTableCell >ROTA (%)</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.Actual_ROTA  && regionData.Actual_ROTA.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.Book_PBT_ROTA  && regionData.Book_PBT_ROTA.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.ROTADiff  && regionData.ROTADiff.toFixed(2):null}</StyledTableCell>
             </StyledTableRow>
             <StyledTableRow key={'3'}>
            <StyledTableCell >NCL (%)</StyledTableCell>
            <StyledTableCell  align="center">{regionData?regionData.Actual_NCL  && regionData.Actual_NCL.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{regionData?regionData.Budget_NCL  && regionData.Budget_NCL.toFixed(2):null}</StyledTableCell>
            <StyledTableCell  align="center">{regionData?regionData.NCLDiff  && regionData.NCLDiff.toFixed(2):null}</StyledTableCell>
            </StyledTableRow>
             <StyledTableRow key={'4'}>
             <StyledTableCell >OPEX (%)</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.OPEX  && regionData.OPEX.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.Budget_OPEX  && regionData.Budget_OPEX.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.OPEXDiff  && parseInt(regionData.OPEXDiff).toFixed(2):null}</StyledTableCell>
             </StyledTableRow>
             <StyledTableRow key={'5'}>
             <StyledTableCell >Yield (%)</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.Actual_IRR && regionData.Actual_IRR.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.Planned_IRR  && regionData.Planned_IRR.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.IRRDiff  && regionData.IRRDiff.toFixed(2):null}</StyledTableCell>
              </StyledTableRow>
             <StyledTableRow key={'6'}>
             <StyledTableCell >Cross Sell (In Lakhs)</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.Actual_Cross_sell_income  && regionData.Actual_Cross_sell_income.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.Planned_Cross_sell_income  && regionData.Planned_Cross_sell_income.toFixed(2):null}</StyledTableCell>
             <StyledTableCell  align="center">{regionData?regionData.CrossSellDiff  && regionData.CrossSellDiff.toFixed(2):null}</StyledTableCell>
 
             </StyledTableRow>
        </TableBody>
      </Table>
    </TableContainer>
     
     
     </div>
    );
  }
}
export default withStyles(styles)(RegionManagerFlow);

