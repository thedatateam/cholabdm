import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//material-ui components
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Slide from '@material-ui/core/Slide';
import * as zoneManagerActions from "../../actions/zoneLevelFlow";
import FormGroup from '@material-ui/core/FormGroup';
import * as areaManagerActions from "../../actions/areaManagerFlow";
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/SearchOutlined';
import CholaLogo from '../../assets/images/cholaLogo.png';

//other libraries
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import classNames from 'classnames';
import { SnackbarProvider, withSnackbar } from 'notistack';
import Pagination from '../Pagination';
function Transition(props) {
  return <Slide direction="up" {...props} />;
}
const styles = theme => ({

  root: {
    height: '100%',
  },
  agheader: {
    backgroundColors: "green"
  },
  formControl: {
    width: '250px',
  },
  tab:{
    paddingLeft:'640px',
  }

});

class ZoneFinalizedRate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      finalizedRateData: [],
      searchfinalizedRateData:[],
      showField: false,
      selectedValue: "",
      error: '',
      zone: '',
      month: '',
      role: '',
      page: 1,
      totalPages: 0,
      columnDefs: [
        { headerName: " AREA", field: "Area", width: 300, suppressMenu: true, tooltipValueGetter: (params) => params.value, },
        { headerName: "REGION", field: "Region", width: 250, sortingOrder: ['asc'], suppressMenu: true, tooltipValueGetter: (params) => params.value, },
        { headerName: "ZONE", field: "Zone", width: 250, suppressMenu: true, tooltipValueGetter: (params) => params.value, },
        { headerName: "PERIOD", field: "Period", width: 250, suppressMenu: true, tooltipValueGetter: (params) => params.value, },
        { headerName: "YEAR", field: "Year", width: 200 },
        { headerName: "SEGMENT", field: "Segment", width: 300 ,tooltipValueGetter: (params) => params.value,},
        { headerName: "NEW/USED", field: "NewOrUsed", width: 300 },
        { headerName: "ASSET DESC", field: "Assetdesc", width: 300, },
        { headerName: "MAKE", field: "Make", width: 400, },
        { headerName: "CATEGORY ", field: "Category", width: 400, },
        { headerName: "PLANNED IRR(LAST MONTH)", field: "Last_Month_Planed_IRR", width: 400, },
        { headerName: "PLANNED IRR(%)", field: "Planned_IRR", width: 250, },
        { headerName: "LAST EDITED BY", field: "Last_Edited_By", width: 250, },
        { headerName: "LAST APPROVED BY", field: "Last_Approved_By", width: 250, },
     ],
     search: '',
      gridOptions: {
        defaultColDef: {
          resizable: true,
          sortable: true,

        },
        cacheBlockSize: 10,
        localeText: { noRowsToShow: 'No Data Found' },
      },
    }
    this.onGridReady = this.onGridReady.bind(this);
    this.handleSummaryPage = this.handleSummaryPage.bind(this);
    this.dropDown = this.dropDown.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);


  };
  componentDidMount() {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    const d = new Date();
    var n = monthNames[d.getMonth()]
    this.setState({ month: n });
    var year = new Date().getFullYear().toString().substr(-2);
    let nextYear = parseInt(year) + 1;
    let role = localStorage.getItem('role');
    this.setState({ role: role })
    let zone = localStorage.getItem('typeValue');
    this.setState({ zone: zone });
    let type = localStorage.getItem('type');
    this.props.actions.getZoneFinalizedRate(zone, 0);
  }
  handleChange(prop, event) {
    this.setState({ [prop]: event.target.value });
  };
  dropDown = event => {
    const { value } = event.target;
    this.setState({
      showField: value ,
      selectedValue: value
    });
  };
  handleTextChange = ( event) => {
    this.setState({ searchedText: event.target.value });
  };
  handleSearch = prop => event => {  
    let area = localStorage.getItem('typeValue');
    this.setState({ [prop]: event.target.value });
    this.props.actions.searchFinalizedRate('Zone',area,this.state.selectedValue,this.state.searchedText);
  };
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  static getDerivedStateFromProps(nextProps, state, props) {
    //console.log("loggin ",nextProps);
    if (nextProps.areaManager.searchFinalizedRateSuccess && nextProps.areaManager.response) {
      nextProps.areaManager.searchFinalizedRateSuccess = false;
    // console.log(nextprops.areaManager.searchfinalizedRate)
      return {
        searchFinalizedRateData: nextProps.areaManager.response.SearchFinalizedRate
      }
  }
  if (nextProps.areaManager.searchfinalizedRateError===true && nextProps.areaManager.searchfinalizedRateErrorStatus) {
      nextProps.areaManager.searchfinalizedRateError = false;
      return {
        searchfinalizedRateData: [],
      }
  }
    if (nextProps.zoneManager.getZoneFinalizedRateSuccess === true && nextProps.zoneManager.response) {
      nextProps.zoneManager.getZoneFinalizedRateSuccess = false;
      return {
        finalizedRateData: nextProps.zoneManager.response.FinalizedRate,
        totalPages: nextProps.zoneManager.response.totalPages
      }
    }
    if (nextProps.zoneManager.getZoneFinalizedRateError === true && nextProps.zoneManager.error) {
      nextProps.zoneManager.getZoneFinalizedRateError = false;
      return {
        finalizedRateData: [],
        totalPages: 0
      }
    }
    return null;
  }
  handleSummaryPage() {
    window.location = '/zoneLevelFlow';
  }
  onPaginationChanged = (page) => {
    const offset = (page - 1) * 1000;
    const zone = localStorage.getItem('typeValue');
    this.setState({ page })
    this.props.actions.getZoneFinalizedRate(zone, offset);

  };
  render() {
    const { classes } = this.props;
    const { selectedValue, showField } = this.state;
    return (
      <div>
        <Grid container>
          <Grid item xs={3}>
            <img src={CholaLogo} style={{ margin: '10px', width: '150px', height: '100px' }} />
          </Grid>
          <Grid item xs={6} style={{ textAlign: 'center', marginTop: '30px' }}>
            <Typography variant='h6' style={{ fontWeight: '250' }}>Finalized Rate
</Typography>
          </Grid>
        </Grid>
        <Grid container justifyContent="space-between">
          <Grid item>
        <Button variant="contained"
          onClick={this.handleSummaryPage}
          style={{ textTransform: 'none',backgroundColor: '#0275d8', margin: '10px', color: 'white' }}>
          Back To Summary      </Button>
          </Grid>
          <Grid item>
          <Grid container style={{ gap: 10, padding: '0 20px' }}>
          <FormControl className={classes.formControl}>
            <InputLabel id="assetType">Enter Value</InputLabel>
            <Select
              labelId="EnterValue"
              id="type"
              name="value"
              value={this.state.selectedValue}
               onChange={this.dropDown}
            >     <MenuItem value={'Area'}>AREA</MenuItem>
                  <MenuItem value={'Region'}>REGION</MenuItem>
                  <MenuItem value={'Zone'}>ZONE</MenuItem>
                  <MenuItem value={'Period'}>PERIOD</MenuItem>
                  <MenuItem value={'Year'}>YEAR</MenuItem>
                  <MenuItem value={'Segment'}>SEGMENT</MenuItem>
                  <MenuItem value={'NewOrUsed'}>NEW/USED</MenuItem>
                  <MenuItem value={'Assetdesc'}>ASSET DESC</MenuItem>
                  <MenuItem value={'Category'}>Category </MenuItem>
                  <MenuItem value={'Make'}>MAKE</MenuItem>
                  <MenuItem value={'Last_Edited_By'}>LAST EDITED BY</MenuItem>
                  <MenuItem value={'Last_Approved_By'}>LAST APPROVED BY</MenuItem>
            </Select>
            </FormControl>&nbsp;&nbsp;{showField ? (
          <TextField
          id="standard-helperText"
          type={'text'}
          onChange={(e) => this.handleTextChange(e)}
          label="Search a Value.."                      
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="Search a user"
                  style={{color:'#007bc9'}}
                  value={this.state.search}
                 onClick={this.handleSearch('search')}
                  autoFocus
                ><SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        /> 
        ) : null} 
        </Grid>
          </Grid>
        </Grid>  
        <Paper elevation={3} style={{ margin: '10px', marginTop: '0px', padding: '10px' }}>
          <div
            className="ag-theme-alpine"
            style={{
              height: '450px',
              width: '100%',
            }}
          >
            <AgGridReact
              rowData={this.state.searchFinalizedRateData && this.state.searchFinalizedRateData.length?this.state.searchFinalizedRateData:this.state.finalizedRateData}
              columnDefs={this.state.columnDefs}
              defaultColDef={this.state.defaultColDef}
              onGridReady={this.onGridReady}
              gridOptions={this.state.gridOptions}

            >
            </AgGridReact>
          </div>
          <Pagination  count={this.state.totalPages} page={this.state.page} pageChange={this.onPaginationChanged} />
        </Paper>
      </div>
    );
  }
}
function actualRotaGetter(params) {
  if (params.data.Actual_ROTA) {
    return (parseInt(params.data.Actual_ROTA).toFixed(2))
  }
  return '0'

}
function plannedRotaGetter(params) {
  if (params.data.Book_PBT_ROTA) {
    return (parseInt(params.data.Book_PBT_ROTA).toFixed(2))
  }
  return '0'
}
function actualIrrGetter(params) {
  if (params.data.Actual_IRR) {
    return (parseInt(params.data.Actual_IRR).toFixed(2))
  }
  return '0'
}
function planneprvMonthIrrGetter(params) {
  if (params.data.Prev_Planned_IRR) {
    return (parseInt(params.data.Prev_Planned_IRR).toFixed(2))
  }
  return '0'
}
function plannedIrrGetter(params) {
  if (params.data.Planned_IRR) {
    return (parseInt(params.data.Planned_IRR).toFixed(2))
  }
  return '0'
}

const mapStateToProps = state => ({
  zoneManager: state.zoneManager,
  areaManager: state.areaManager
});

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...zoneManagerActions,...areaManagerActions }, dispatch)

  };
};

const WrappedZoneFinalizedRate = withMobileDialog()(withStyles(styles)(withSnackbar(ZoneFinalizedRate)));

class ZoneFinalizedRateWithSnackBar extends React.Component {
  render() {
    return (
      <SnackbarProvider
        maxSnack={10}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}>
        <WrappedZoneFinalizedRate {...this.props} />
      </SnackbarProvider>
    );
  }
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ZoneFinalizedRateWithSnackBar);
