import { Button, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import {
  submitForApprovalRM,
  submitForReworkRM,
  submitForReworkCommentRM,
} from '../../actions/submitForApproval';

const useStyles = makeStyles({
  ctaBtn: {
    backgroundColor: '#0275d8',
    margin: '10px 0',
    color: '#fff',
    '&:hover': {
      backgroundColor: '#0275d8',
    },
  },
});

const ApproveReworkButton = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [openSuccess, setOpenSuccess] = React.useState(false);
  const [openSuccess1, setOpenSuccess1] = React.useState(false);
  const submitForApprovalState = useSelector(
    (state) => state.submitForApproval
  );

  React.useEffect(() => {
    if (submitForApprovalState.statusRM === 'SUCCESS') {
      setOpen(false);
      setOpenSuccess1(true);
      localStorage.setItem('AMreworkStatus', 0);
      localStorage.setItem('rmSubmitVal', 1);
    }
  }, [submitForApprovalState.statusRM]);

  React.useEffect(() => {
    if (
      submitForApprovalState.statusRRM === 'SUCCESS' &&
      submitForApprovalState.statusRRCM === 'SUCCESS'
    ) {
      setOpenSuccess(true);
      localStorage.setItem('RMreworkStatus', 1);
      localStorage.setItem('RMreworkButtonClick', 1);
      localStorage.setItem('zmSubmitVal', 0);
    }
  }, [submitForApprovalState.statusRRM, submitForApprovalState.statusRRCM]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function handleCloseSuccess() {
    setOpenSuccess(false);
  }

  function handleCloseSuccess1() {
    setOpenSuccess1(false);
  }

  function handleSubmitApproval() {
    setOpenSuccess1(false);
    dispatch(submitForApprovalRM());
  }

  function handleRework() {
    setOpenSuccess(false);
    let region = localStorage.getItem('typeValue');

    const body = {
      Name: region,
      Type: 'Region',
      Comment: '',
    };

    dispatch(submitForReworkCommentRM(body));
    dispatch(submitForReworkRM());
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item>
          <Button
            style={{ textTransform: 'none' }}
            variant="contained"
            className={classes.ctaBtn}
            onClick={handleClickOpen}
          >
            Approve
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            style={{ textTransform: 'none' }}
            className={classes.ctaBtn}
            onClick={handleRework}
            disabled={
              submitForApprovalState.statusRRM === 'PENDING' ||
              submitForApprovalState.statusRRCM === 'PENDING'
            }
          >
            Rework
          </Button>
        </Grid>
      </Grid>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {'Are you sure to submit the data?'}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmitApproval} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openSuccess}
        onClose={handleCloseSuccess}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Rework assigned successfully.
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseSuccess} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openSuccess1}
        onClose={handleCloseSuccess1}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Approved Successfully.
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseSuccess1} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ApproveReworkButton;
