import { Button } from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import { submitForApproval } from '../../actions/submitForApproval';

const SubmitForApproval = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [openSuccess, setOpenSuccess] = React.useState(false);
  const submitForApprovalState = useSelector((state) => state.submitForApproval);

  React.useEffect(() => {
    if (submitForApprovalState.status === 'SUCCESS') {
      setOpen(false);
      setOpenSuccess(true);
    }
  }, [submitForApprovalState]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function handleSubmitApproval() {
    dispatch(submitForApproval());
  }

  function handleCloseSuccess() {
    setOpenSuccess(false);
  }

  return (
    <>
      <Button
        variant="contained"
        style={{textTransform: 'none', backgroundColor: '#0275d8', margin: '10px', color: 'white' }}
        onClick={handleClickOpen}
      >
        Submit For Approval
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {'Submit for Approval'}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmitApproval} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openSuccess}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {'Submitted Successfully.'}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseSuccess} color="primary" autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default SubmitForApproval;
